package util;

import io.appium.java_client.MobileElement;
import io.qameta.allure.model.Status;
import org.openqa.selenium.NoSuchElementException;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.left;
import static org.apache.commons.lang3.StringUtils.trim;

public class MetodosGenericos extends utils.MetodosGenericos {

    public static void compararTextoConReporte(MobileElement objeto, String textoEsperado)
    {
        try{
            boolean existeObjeto = visualizarObjeto(objeto,5);
            if(existeObjeto==true)
            {
                if (objeto.getText().equals(textoEsperado))
                {
                    PdfBciReports.addMobilReportImage( "compararTextoConReporte","Se visualiza objeto con texto esperado:'"+textoEsperado+"'.", EstadoPrueba.PASSED, false);
                    Reports.addStep("Se visualiza objeto con texto esperado:'"+textoEsperado+"'.",true, Status.PASSED,false);
                }else{
                    PdfBciReports.addMobilReportImage( "compararTextoConReporte","Se visualiza objeto con texto:'"+objeto.getText()+"', mientras se esperaba:'"+textoEsperado+"'", EstadoPrueba.FAILED, false);
                    Reports.addStep("Se visualiza objeto con texto:'"+objeto.getText()+"', mientras se esperaba:'"+textoEsperado+"'",true, Status.FAILED,false);
                }
            }else{
                PdfBciReports.addMobilReportImage( "compararTextoConReporte","No se visualiza objeto en la vista desplegada.", EstadoPrueba.FAILED, false);
                Reports.addStep("No se visualiza objeto en la vista desplegada.",true, Status.FAILED,false);
            }
        }catch(Exception e){
            System.out.println("No se  logra ejecutar funcion validarTexto, motivo:"+e.getMessage());
            PdfBciReports.addMobilReportImage( "compararTextoConReporte","Error en el metodo 'validarTexto', motivo:"+e.getMessage(), EstadoPrueba.FAILED, true);
            Reports.addStep("Error en el metodo 'validarTexto', motivo:"+e.getMessage(),true,Status.FAILED,true);
        }
    }

    public static void compararTextoIncompletoConReporte(MobileElement objeto, String textoEsperado)
    {
        try{
            boolean existeObjeto = visualizarObjeto(objeto,5);
            if(existeObjeto==true)
            {
                if (objeto.getText().contains(textoEsperado))
                {
                    PdfBciReports.addMobilReportImage( "compararTextoIncompletoConReporte","Se visualiza objeto que contiene texto esperado:'"+textoEsperado+"'.", EstadoPrueba.PASSED, false);
                    Reports.addStep("Se visualiza objeto que contiene texto esperado:'"+textoEsperado+"'.",true, Status.PASSED,false);
                }else{
                    PdfBciReports.addMobilReportImage( "compararTextoIncompletoConReporte","Se visualiza objeto con texto:'"+objeto.getText()+"', mientras se esperaba:'"+textoEsperado+"'", EstadoPrueba.FAILED, false);
                    Reports.addStep("Se visualiza objeto con texto:'"+objeto.getText()+"', mientras se esperaba:'"+textoEsperado+"'",true, Status.FAILED,false);
                }
            }else{
                PdfBciReports.addMobilReportImage( "compararTextoIncompletoConReporte","No se visualiza objeto en la vista desplegada.", EstadoPrueba.FAILED, false);
                Reports.addStep("No se visualiza objeto en la vista desplegada.",true, Status.FAILED,false);
            }
        }catch(Exception e){
            System.out.println("No se  logra ejecutar funcion validarTexto, motivo:"+e.getMessage());
            PdfBciReports.addMobilReportImage( "compararTextoIncompletoConReporte","Error en el metodo 'validarTexto', motivo:"+e.getMessage(), EstadoPrueba.FAILED, true);
            Reports.addStep("Error en el metodo 'validarTexto', motivo:"+e.getMessage(),true,Status.FAILED,true);
        }
    }

    public static boolean validaAlias(String alias) {
        //
        String regexp = "Hola [A-Z a-z 0-9]+";
        return Pattern.matches(regexp, alias);
    }

    public static String rutParaQuery(String rut){
        rut = rut.replace("-", "");
        rut = rut.replace(".", "");
        rut = trim(rut);
        rut = left(rut, (rut.length()-1));
        if(rut.length() == 8)
            rut = "0"+rut;
        else if(rut.length() == 7)
            rut = "00"+rut;
        return rut;
    }

    public static boolean isChecked(MobileElement element) throws NoSuchElementException {
        boolean existeObjeto = visualizarObjeto(element,5);
        if(existeObjeto==true)
        {
            System.out.println("Está el elemento con checked:" + element.getAttribute("checked"));
        }else{
            PdfBciReports.addMobilReportImage( "compararTextoIncompletoConReporte","No se visualiza objeto en la vista desplegada.", EstadoPrueba.FAILED, false);
            Reports.addStep("No se visualiza objeto en la vista desplegada.",false, Status.FAILED,false);
        }
        return element.getAttribute("checked").trim().equals("true");
    }
}