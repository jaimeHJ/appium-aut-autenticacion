package testSuites.ReleaseUnboxingIOS;

import constants.OS;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import testClasses.Unboxing.HuellaDigital.*;

import static ALM.UploadEvidenceALM.setTestInstanceID;
import static ALM.UploadEvidenceALM.uploadALM;


public class release317IOS {

    @BeforeMethod
    public void setUp()
    {
        PdfBciReports.createPDF();

        //DriverContext.setUp(OS.IOS,"9eaa12dc51b5a07a1a378b27f118d8646dc2342f","iPhone SE",false,"AppPersonasBci");
        //DriverContext.setUp(OS.ANDROID,"520089f95b3515a9","device", false,"app-qa");
        //public void setUp(){ DriverContext.setUp(OS.ANDROID,"ad051703a8b5cad32b","Samsung Galaxy S7 edge", false,"app-qa");}

        //ABM
        DriverContext.setUp(OS.IOS,"bfd8bbd0d5707445a9fe744bf8bebf9018a45087","iPhone 7 Autenticación",false,"AppPersonasBciPruebas");
        //DriverContext.setUp(OS.IOS,"8d60ec46e8607aa564c689fbdb5018bc7a423c44","iPhone X",false,"AppPersonasBciPruebas");
        //DriverContext.setUp(OS.IOS,"28636daf117871f22869fa89499161aeb74158ea","iPhone 7 Transversal",false,"AppPersonasBciPruebas");
    }

    @AfterMethod
    public void tearDown()
    {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test //OK // C2 OK
    public void CPA00047AutorizarHuellaDigitalClienteconAliasRegistrado() throws Exception
    {
        try
        {
            setTestInstanceID("34889","almAutenticacion.properties");
            CPA00047AutorizarHuellaDigitalClienteconAliasRegistrado cpa00047AutorizarHuellaDigitalClienteconAliasRegistrado = new CPA00047AutorizarHuellaDigitalClienteconAliasRegistrado();
            cpa00047AutorizarHuellaDigitalClienteconAliasRegistrado.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00047AutorizarHuellaDigitalClienteconAliasRegistrado","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK
    public void CPA00048AutorizarHuellaDigitalClienteSinAliasRegistrado() throws Exception
    {
        try
        {
            setTestInstanceID("34865","almAutenticacion.properties");
            CPA00048AutorizarHuellaDigitalClienteSinAliasRegistrado cpa00048AutorizarHuellaDigitalClienteSinAliasRegistrado = new CPA00048AutorizarHuellaDigitalClienteSinAliasRegistrado();
            cpa00048AutorizarHuellaDigitalClienteSinAliasRegistrado.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00048AutorizarHuellaDigitalClienteSinAliasRegistrado","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK // C2 OK
    public void CPA00049OmitirAutorizaciondeHuellaDigitalConAlias() throws Exception
    {
        try
        {
            setTestInstanceID("34891","almAutenticacion.properties");
            CPA00049OmitirAutorizaciondeHuellaDigitalConAlias cpa00049OmitirAutorizaciondeHuellaDigitalConAlias = new CPA00049OmitirAutorizaciondeHuellaDigitalConAlias();
            cpa00049OmitirAutorizaciondeHuellaDigitalConAlias.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00049OmitirAutorizaciondeHuellaDigitalConAlias","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK
    public void CPA00050OmitirAutorizaciondeHuellaDigitalSinAlias() throws Exception
    {
        try
        {
            setTestInstanceID("34867","almAutenticacion.properties");
            CPA00050OmitirAutorizaciondeHuellaDigitalSinAlias cpa00050OmitirAutorizaciondeHuellaDigitalSinAlias = new CPA00050OmitirAutorizaciondeHuellaDigitalSinAlias();
            cpa00050OmitirAutorizaciondeHuellaDigitalSinAlias.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00050OmitirAutorizaciondeHuellaDigitalSinAlias","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK
    public void CPA00055RechazarAutorizaciondeHuellaDispositivosNoENrrolado() throws Exception
    {
        try
        {
            setTestInstanceID("34868","almAutenticacion.properties");
            CPA00055RechazarAutorizaciondeHuellaDispositivosNoENrrolado cpa00055RechazarAutorizaciondeHuellaDispositivosNoENrrolado = new CPA00055RechazarAutorizaciondeHuellaDispositivosNoENrrolado();
            cpa00055RechazarAutorizaciondeHuellaDispositivosNoENrrolado.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00055RechazarAutorizaciondeHuellaDispositivosNoENrrolado","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK // C2 OK
    public void CPA00056AutorizarFaceIDClienteconAliasRegistrado() throws Exception
    {
        try
        {
            setTestInstanceID("34894","almAutenticacion.properties");
            CPA00056AutorizarFaceIDClienteconAliasRegistrado CPA00056AutorizarFaceIDClienteconAliasRegistrado = new CPA00056AutorizarFaceIDClienteconAliasRegistrado();
            CPA00056AutorizarFaceIDClienteconAliasRegistrado.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00056AutorizarFaceIDClienteconAliasRegistrado","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK
    public void CPA00057AutorizarFaceIDClienteSinAliasRegistrado() throws Exception
    {
        try
        {
            setTestInstanceID("34875","almAutenticacion.properties");
            CPA00057AutorizarFaceIDClienteSinAliasRegistrado CPA00057AutorizarFaceIDClienteSinAliasRegistrado = new CPA00057AutorizarFaceIDClienteSinAliasRegistrado();
            CPA00057AutorizarFaceIDClienteSinAliasRegistrado.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00057AutorizarFaceIDClienteSinAliasRegistrado","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK
    public void CPA00058OmitirAutorizacionDeFaceIDConAlias() throws Exception
    {
        try
        {
            setTestInstanceID("34870","almAutenticacion.properties");
            CPA00058OmitirAutorizacionDeFaceIDConAlias CPA00058OmitirAutorizacionDeFaceIDConAlias = new CPA00058OmitirAutorizacionDeFaceIDConAlias();
            CPA00058OmitirAutorizacionDeFaceIDConAlias.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00058OmitirAutorizacionDeFaceIDConAlias","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK // C2
    public void CPA00059OmitirAutorizacionDeFaceIDSinAlias() throws Exception
    {
        try
        {
            setTestInstanceID("34896","almAutenticacion.properties");
            CPA00059OmitirAutorizacionDeFaceIDSinAlias CPA00059OmitirAutorizacionDeFaceIDSinAlias = new CPA00059OmitirAutorizacionDeFaceIDSinAlias();
            CPA00059OmitirAutorizacionDeFaceIDSinAlias.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00059OmitirAutorizacionDeFaceIDSinAlias","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK
    public void CPA00060RechazarAutorizacionDeFaceIDDispositivoNoEnrolado() throws Exception
    {
        try
        {
            setTestInstanceID("34872","almAutenticacion.properties");
            CPA00060RechazarAutorizacionDeFaceIDDispositivoNoEnrolado CPA00060RechazarAutorizacionDeFaceIDDispositivoNoEnrolado = new CPA00060RechazarAutorizacionDeFaceIDDispositivoNoEnrolado();
            CPA00060RechazarAutorizacionDeFaceIDDispositivoNoEnrolado.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00060RechazarAutorizacionDeFaceIDDispositivoNoEnrolado","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK // C2
    public void CPA00063AutorizarHuellaDesdeMenu() throws Exception
    {
        try
        {
            setTestInstanceID("34898","almAutenticacion.properties");
            CPA00063AutorizarHuellaDesdeMenu CPA00063AutorizarHuellaDesdeMenu = new CPA00063AutorizarHuellaDesdeMenu();
            CPA00063AutorizarHuellaDesdeMenu.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00063AutorizarHuellaDesdeMenu","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test //OK
    public void CPA00064DesactivarHuellaDesdeMenu() throws Exception
    {
        try
        {
            setTestInstanceID("34874","almAutenticacion.properties");
            CPA00064DesactivarHuellaDesdeMenu CPA00064DesactivarHuellaDesdeMenu = new CPA00064DesactivarHuellaDesdeMenu();
            CPA00064DesactivarHuellaDesdeMenu.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00064DesactivarHuellaDesdeMenu","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }
}
