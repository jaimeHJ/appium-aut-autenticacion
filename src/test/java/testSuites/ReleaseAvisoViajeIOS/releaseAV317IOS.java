package testSuites.ReleaseAvisoViajeIOS;

import constants.OS;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.AvisoDeViaje.*;

import static ALM.UploadEvidenceALM.setTestInstanceID;
import static ALM.UploadEvidenceALM.uploadALM;

public class releaseAV317IOS
{
    @BeforeMethod
    public void setUp()
    {
        PdfBciReports.createPDF();

        //ABM
        //DriverContext.setUp(OS.IOS,"bfd8bbd0d5707445a9fe744bf8bebf9018a45087","Iphone 7 Autenticación",false,"AppPersonasBciPruebas");
        DriverContext.setUp(OS.IOS,"8d60ec46e8607aa564c689fbdb5018bc7a423c44","iPhone X",false,"AppPersonasBciPruebas");
        //DriverContext.setUp(OS.ANDROID, "ZY322756Z6", "Moto G5", false, "app-integration.apk");
    }

    @AfterMethod
    public void tearDown()
    {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test
    public void CPA00001InformarAvisoDeViajeAlExtranjero() throws Exception
    {
        try
        {
            setTestInstanceID("34888","almAutenticacion.properties");
            CPA00001InformarAvisoDeViajeAlExtranjero CPA00001InformarAvisoDeViajeAlExtranjero = new CPA00001InformarAvisoDeViajeAlExtranjero();
            CPA00001InformarAvisoDeViajeAlExtranjero.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00001InformarAvisoDeViajeAlExtranjero","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test
    public void CPA00002RechazarInformarAvisoDeViajeAlExtranjero() throws Exception
    {
        try
        {
            setTestInstanceID("34887","almAutenticacion.properties");
            CPA00002RechazarInformarAvisoDeViajeAlExtranjero CPA00002RechazarInformarAvisoDeViajeAlExtranjero = new CPA00002RechazarInformarAvisoDeViajeAlExtranjero();
            CPA00002RechazarInformarAvisoDeViajeAlExtranjero.pruebaAutomatizada();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00002RechazarInformarAvisoDeViajeAlExtranjero","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

}
