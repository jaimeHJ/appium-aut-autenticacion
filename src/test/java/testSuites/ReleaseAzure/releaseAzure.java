package testSuites.ReleaseAzure;

import constants.OS;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.Login.LoginNormal.CPA00029DesplegarAppBCIConNuevoLogin;
import testClasses.Login.LoginNormal.CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI;

import static ALM.UploadEvidenceALM.uploadALM;

public class releaseAzure {

    @BeforeMethod
    public void setUp()
    {
        PdfBciReports.createPDF();

        //DriverContext.setUp(OS.IOS,"9eaa12dc51b5a07a1a378b27f118d8646dc2342f","iPhoneSE",false,"AppPersonasBci");
        DriverContext.setUp(OS.IOS,"8d60ec46e8607aa564c689fbdb5018bc7a423c44","iPhone X",false,"AppPersonasBci");
        //DriverContext.setUp(OS.ANDROID,"520089f95b3515a9","device", false,"app-qa");
        //public void setUp(){ DriverContext.setUp(OS.ANDROID,"ad051703a8b5cad32b","Samsung Galaxy S7 edge", false,"app-qa");}
    }

    @AfterMethod
    public void tearDown()
    {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test
    public void CPA00029DesplegarAppBCIConNuevoLogin() throws Exception
    {
        CPA00029DesplegarAppBCIConNuevoLogin cpa00029DesplegarAppBCIConNuevoLogin = new CPA00029DesplegarAppBCIConNuevoLogin();
        cpa00029DesplegarAppBCIConNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI() throws Exception
    {
        CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI = new CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI();
        cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }
}
