package testSuites;

import constants.OS;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.Login.LoginAlias.CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI;
import testClasses.Login.LoginAlias.CPA00002IngresarConAliasAAppBCIConNuevoLogin;
import testClasses.Login.LoginAlias.CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin;
import testClasses.Login.LoginHuella.CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin;
import testClasses.Login.LoginNormal.CPA00029DesplegarAppBCIConNuevoLogin;
import testClasses.Login.LoginNormal.CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez;
import testClasses.Login.LoginNormal.CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI;
import testClasses.Login.TerminosyCondiciones.CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin;
import testbase.TestBase;

import static ALM.UploadEvidenceALM.setTestInstanceID;
import static ALM.UploadEvidenceALM.uploadALM;

public class iOSRutaCritica extends TestBase
{
    @BeforeMethod
    public void setUp()
    {
        PdfBciReports.createPDF();

        //DriverContext.setUp(OS.IOS,"9eaa12dc51b5a07a1a378b27f118d8646dc2342f","iPhone SE MLLP2CI/2 Autenticación", false,"AppPersonasBci");
        //DriverContext.setUp(OS.IOS,"8d60ec46e8607aa564c689fbdb5018bc7a423c44","iPhone X",false,"AppPersonasBciPruebas");
        DriverContext.setUp(OS.IOS,"bfd8bbd0d5707445a9fe744bf8bebf9018a45087","iPhone 7 Autenticación",false,"AppPersonasBciPruebas");

    }

    @AfterMethod
    public void tearDown()
    {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test // NO ESTA EN ALM
    public void CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI() throws Exception
    {
        try
        {
            setTestInstanceID("34787","almAutenticacion.properties");
            loginRapido("10042323-5","111222");
            CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI = new CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI();
            cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test // NO ESTA EN ALM
    public void CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin() throws Exception
    {
        try
        {
            setTestInstanceID("","almAutenticacion.properties");
            CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin cpa00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin = new CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin();
            cpa00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test // OK
    public void CPA00002IngresarConAliasAAppBCIConNuevoLogin() throws Exception
    {
        try
        {
            setTestInstanceID("34882","almAutenticacion.properties");
            loginRapido("10042323-5","111222");
            CPA00002IngresarConAliasAAppBCIConNuevoLogin cpa00002IngresarConAliasAAppBCIConNuevoLogin = new CPA00002IngresarConAliasAAppBCIConNuevoLogin();
            cpa00002IngresarConAliasAAppBCIConNuevoLogin.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00002IngresarConAliasAAppBCIConNuevoLogin","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test // NO ESTA EN ALM
    public void CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin() throws Exception
    {
        setTestInstanceID("34848","almAutenticacion.properties");
        loginRapido("10042323-5","111222");
        CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin cpa00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin = new CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin();
        cpa00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test // 31 EN ALM - OK
    public void CPA00029DesplegarAppBCIConNuevoLogin() throws Exception
    {
        try
        {
            setTestInstanceID("34694","almAutenticacion.properties");
            CPA00029DesplegarAppBCIConNuevoLogin cpa00029DesplegarAppBCIConNuevoLogin = new CPA00029DesplegarAppBCIConNuevoLogin();
            cpa00029DesplegarAppBCIConNuevoLogin.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00029DesplegarAppBCIConNuevoLogin","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test // 32 EN ALM - OK // C2
    public void CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez() throws Exception
    {
        try
        {
            setTestInstanceID("34903","almAutenticacion.properties");
            CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez cpa00030IngresarAAppBCIConNuevoLoginPorPrimeraVez = new CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez();
            cpa00030IngresarAAppBCIConNuevoLoginPorPrimeraVez.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    @Test // 36 EN ALM - FALLIDO // C2 FALLIDO
    public void CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI() throws Exception
    {
        try
        {
            setTestInstanceID("34904","almAutenticacion.properties");
            CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI= new CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI();
            cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI.pruebaAutomatizada();
            PdfBciReports.closePDF();
            Reports.finalAssert();
        }catch (NullPointerException ex)
        {
            PdfBciReports.addMobilReportImage("CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI","Falla de Script, Detalle:" + ex.getMessage(), EstadoPrueba.FAILED, true);
        }
    }

    /*@Test
    public void CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin() throws Exception
    {
        setTestInstanceID("","almAutenticacion.properties");
        CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin cpa00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin = new CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin();
        cpa00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }*/
}
