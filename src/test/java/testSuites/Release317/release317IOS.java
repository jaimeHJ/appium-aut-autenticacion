package testSuites.Release317;

import constants.OS;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.Login.LoginAlias.CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI;
import testClasses.Login.LoginAlias.CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin;
import testClasses.Login.LoginAlias.CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias;
import testClasses.Login.LoginAlias.CPA00007RechazarIngresoconClaveVencida;
import testClasses.Login.LoginNormal.CPA00029DesplegarAppBCIConNuevoLogin;
import testClasses.Login.LoginNormal.CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI;
import testClasses.Login.LoginNormal.CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci;
import testClasses.Login.LoginNormal.CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci;

import static ALM.UploadEvidenceALM.setTestInstanceID;
import static ALM.UploadEvidenceALM.uploadALM;

public class release317IOS {

    @BeforeMethod
    public void setUp()
    {
        PdfBciReports.createPDF();

        //DriverContext.setUp(OS.IOS,"9eaa12dc51b5a07a1a378b27f118d8646dc2342f","iPhoneSE",false,"AppPersonasBci");
        //DriverContext.setUp(OS.IOS,"8d60ec46e8607aa564c689fbdb5018bc7a423c44","iPhone X",false,"AppPersonasBci");
        //DriverContext.setUp(OS.ANDROID,"520089f95b3515a9","device", false,"app-qa");
        //public void setUp(){ DriverContext.setUp(OS.ANDROID,"ad051703a8b5cad32b","Samsung Galaxy S7 edge", false,"app-qa");}

        DriverContext.setUp(OS.IOS,"8d60ec46e8607aa564c689fbdb5018bc7a423c44","iPhone X",false,"AppPersonasBci PRUEBAS");
    }

    @AfterMethod
    public void tearDown()
    {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test
    public void CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI() throws Exception
    {
        setTestInstanceID("34787","almAutenticacion.properties");
        CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI = new CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI();
        cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test //ERROR VALIDACION OBJETO Y PDF.
    public void CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin() throws Exception
    {
        setTestInstanceID("34815","almAutenticacion.properties");
        CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin cpa00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin = new CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin();
        cpa00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test //ERROR VALIDACION OBJETO Y PDF.
    public void CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias() throws Exception
    {
        setTestInstanceID("34818","almAutenticacion.properties");
        CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias cpa00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias = new CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias();
        cpa00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test //ERROR VALIDACION OBJETO Y PDF.
    public void CPA00007RechazarIngresoconClaveVencida() throws Exception
    {
        setTestInstanceID("34819","almAutenticacion.properties");
        CPA00007RechazarIngresoconClaveVencida cpa00007RechazarIngresoconClaveVencida = new CPA00007RechazarIngresoconClaveVencida();
        cpa00007RechazarIngresoconClaveVencida.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00029DesplegarAppBCIConNuevoLogin() throws Exception
    {
        setTestInstanceID("34810","almAutenticacion.properties");
        CPA00029DesplegarAppBCIConNuevoLogin cpa00029DesplegarAppBCIConNuevoLogin = new CPA00029DesplegarAppBCIConNuevoLogin();
        cpa00029DesplegarAppBCIConNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test //ERROR VALIDACION OBJETO Y PDF.
    public void CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI() throws Exception
    {
        setTestInstanceID("34820","almAutenticacion.properties");
        CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI = new CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI();
        cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci() throws Exception
    {
        CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci cpa00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci = new CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci();
        cpa00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci() throws Exception
    {
        CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci cpa00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci = new CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci();
        cpa00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }
}
