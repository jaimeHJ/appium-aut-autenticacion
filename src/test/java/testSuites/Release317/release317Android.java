package testSuites.Release317;

import constants.OS;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.Login.LoginAlias.*;
import testClasses.Login.LoginHuella.*;
import testClasses.Login.LoginNormal.*;
import testClasses.Login.TerminosyCondiciones.*;
import testbase.TestBase;

import static ALM.UploadEvidenceALM.uploadALM;

public class release317Android extends TestBase
{
    @BeforeMethod
    public void setUp()
    {
        PdfBciReports.createPDF();

        //DriverContext.setUp(OS.ANDROID,"LBY9X18507G05192","huawei", false,"app-qaedb2");
        DriverContext.setUp(OS.ANDROID,"520089f95b3515a9","Samsung A8", false,"app-qatdc2");
        //public void setUp(){ DriverContext.setUp(OS.ANDROID,"ad051703a8b5cad32b","Samsung Galaxy S7 edge", false,"app-qa");}
    }

    @AfterMethod
    public void tearDown()
    {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test
    public void CPA00029DesplegarAppBCIConNuevoLogin() throws Exception
    {
        CPA00029DesplegarAppBCIConNuevoLogin cpa00029DesplegarAppBCIConNuevoLogin = new CPA00029DesplegarAppBCIConNuevoLogin();
        cpa00029DesplegarAppBCIConNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }


    @Test
    public void CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI() throws Exception
    {
        CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI = new CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI();
        cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin() throws Exception
    {
        CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin cpa00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin = new CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin();
        cpa00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias() throws Exception
    {
        CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias cpa00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias = new CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias();
        cpa00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00007RechazarIngresoconClaveVencida() throws Exception
    {
        CPA00007RechazarIngresoconClaveVencida cpa00007RechazarIngresoconClaveVencida = new CPA00007RechazarIngresoconClaveVencida();
        cpa00007RechazarIngresoconClaveVencida.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI() throws Exception
    {
        CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI = new CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI();
        cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci() throws Exception
    {
        CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci cpa00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci = new CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci();
        cpa00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }

    @Test
    public void CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci() throws Exception
    {
        CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci cpa00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci = new CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci();
        cpa00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }
}
