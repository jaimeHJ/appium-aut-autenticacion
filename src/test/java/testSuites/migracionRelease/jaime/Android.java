package testSuites.migracionRelease.jaime;

import constants.OS;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.Login.TerminosyCondiciones.CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin;
import testClasses.Login.TerminosyCondiciones.CPA00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin;
import testClasses.Login.TerminosyCondiciones.CPA00029AceptarTerminosyCondicionesActualizadosEnAppBCI;
import testbase.TestBase;

public class Android extends TestBase
{
    @BeforeMethod
    public void setUp()
    {
        PdfBciReports.createPDF();

        DriverContext.setUp(OS.ANDROID,"192.168.44.101:5555","device", true,"app-qa");

//        DriverContext.setUp(OS.ANDROID,"KPS4C19301001447","huawei", false,"release317tdc");
//        DriverContext.setUp(OS.ANDROID,"520089f95b3515a9","device", false,"3-17v3");
//        public void setUp(){ DriverContext.setUp(OS.ANDROID,"ad051703a8b5cad32b","Samsung Galaxy S7 edge", false,"app-qa");}
    }

    @AfterMethod
    public void tearDown()
    {
//        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test
    public void CPA00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin() throws Exception
    {
        CPA00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin cpa00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin = new CPA00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin();
        cpa00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }
    @Test
    public void CPA00029AceptarTerminosyCondicionesActualizadosEnAppBCI() throws Exception
    {
        CPA00029AceptarTerminosyCondicionesActualizadosEnAppBCI cpa00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin = new CPA00029AceptarTerminosyCondicionesActualizadosEnAppBCI();
        cpa00027AceptarTerminosyCondicionesLoginPorPrimeraVezEnAppBCIConNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }





//    @Test
//    public void CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI() throws Exception
//    {
//        CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI = new CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI();
//        cpa00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI.pruebaAutomatizada();
//        PdfBciReports.closePDF();
//        Reports.finalAssert();
//    }
//
    @Test
    public void CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin() throws Exception
    {
        CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin cpa00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin = new CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin();
        cpa00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin.pruebaAutomatizada();
        PdfBciReports.closePDF();
        Reports.finalAssert();
    }
//
//    @Test
//    public void CPA00002IngresarConAliasAAppBCIConNuevoLogin() throws Exception
//    {
//        loginRapido("10042323-5","111222");
//        CPA00002IngresarConAliasAAppBCIConNuevoLogin cpa00002IngresarConAliasAAppBCIConNuevoLogin = new CPA00002IngresarConAliasAAppBCIConNuevoLogin();
//        cpa00002IngresarConAliasAAppBCIConNuevoLogin.pruebaAutomatizada();
//        PdfBciReports.closePDF();
//        Reports.finalAssert();
//    }
////
//    @Test
//    public void CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin() throws Exception
//    {
//        CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin cpa00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin = new CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin();
//        cpa00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin.pruebaAutomatizada();
//        PdfBciReports.closePDF();
//        Reports.finalAssert();
//    }
//
//    @Test
//    public void CPA00029DesplegarAppBCIConNuevoLogin() throws Exception
//    {
//        CPA00029DesplegarAppBCIConNuevoLogin cpa00029DesplegarAppBCIConNuevoLogin = new CPA00029DesplegarAppBCIConNuevoLogin();
//        cpa00029DesplegarAppBCIConNuevoLogin.pruebaAutomatizada();
//        PdfBciReports.closePDF();
//        Reports.finalAssert();
//    }
//
//    @Test
//    public void CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez() throws Exception
//    {
//        CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez cpa00030IngresarAAppBCIConNuevoLoginPorPrimeraVez = new CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez();
//        cpa00030IngresarAAppBCIConNuevoLoginPorPrimeraVez.pruebaAutomatizada();
//        PdfBciReports.closePDF();
//        Reports.finalAssert();
//    }
//
//    @Test
//    public void CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin() throws Exception
//    {
//        CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin cpa00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin = new CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin();
//        cpa00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin.pruebaAutomatizada();
//        PdfBciReports.closePDF();
//        Reports.finalAssert();
//    }
//
//    @Test
//    public void CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI() throws Exception
//    {
//        CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI = new CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI();
//        cpa00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI.pruebaAutomatizada();
//        PdfBciReports.closePDF();
//        Reports.finalAssert();
//    }

}
