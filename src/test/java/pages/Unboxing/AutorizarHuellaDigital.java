package pages.Unboxing;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;

import static util.MetodosGenericos.compararTextoConReporte;

public class AutorizarHuellaDigital
{
    AppiumDriver driver;

    public AutorizarHuellaDigital()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    //titulo 1
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Inicia sesión con tu huella")
    public MobileElement lblTitulo1;

    //descripcion 1
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Ingresa de manera más rápida y segura usando las huellas registradas en tu teléfono.")
    public MobileElement lblDescripcion1;

    //Cambiar id
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "¡Y no lo olvides!")
    public MobileElement lblTitulo2;

    //Cambiar id
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Todas las huellas registradas en tu teléfono servirán para acceder a la aplicación.")
    public MobileElement lblDescripcion2;

    //Cambiar id
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Omitir")
    public MobileElement btnOmitir;
    //Cambiar id
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "AUTORIZAR HUELLA")
    public MobileElement btnAutorizarHuella;
    //Cambiar id
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "¿Sabes cuáles son tus huellas registradas?")
    public MobileElement btnHuellasRegistradas;

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "En otro momento")
    public MobileElement btnEnOtroMomento;

    //===========METODOS==========================================================================================================================

    @Step ("Validar Objetos pantalla Autorizar Huella.")
    public void validarObjetosAutorizaeHuella(){

        compararTextoConReporte(btnOmitir, "Omitir");
        compararTextoConReporte(lblTitulo2, "¡Y no lo olvides!");
        compararTextoConReporte(lblDescripcion2, "Todas las huellas registradas en tu teléfono servirán para acceder a la aplicación.");
        compararTextoConReporte(btnAutorizarHuella, "AUTORIZAR HUELLA");
        compararTextoConReporte(btnHuellasRegistradas,"¿Sabes cuáles son tus huellas registradas?");
    }

    @Step ("Validar Objetos Autorizar Huella desde Menu.")
    public void validarObjetosAutorizaeHuellaDesdeMenu()
    {
        compararTextoConReporte(lblTitulo2, "¡Y no lo olvides!");
        compararTextoConReporte(lblDescripcion2, "Todas las huellas registradas en tu teléfono servirán para acceder a la aplicación.");
        compararTextoConReporte(btnAutorizarHuella, "AUTORIZAR HUELLA");
        compararTextoConReporte(btnHuellasRegistradas,"¿Sabes cuáles son tus huellas registradas?");
    }

    @Step ("Presionar en el Boton Autorizar Huella.")
    public void presionarAutorizaHuella(){ btnAutorizarHuella.click(); }

    @Step ("Presionar Link Huellas Registradas.")
    public void presionarHuellasRegistradas(){ btnHuellasRegistradas.click(); }

    @Step ("Presionar Boton Omitir.")
    public void presionarOmitir () { btnOmitir.click(); }

    @Step ("Presionar Boton En otro momento. Antiguo.")
    public void presionarEnOtroMomento () { btnEnOtroMomento.click(); }
}
