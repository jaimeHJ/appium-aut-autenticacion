package pages.Unboxing;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.compararTextoConReporte;

public class ModalRegistrodeHuellas
{
    AppiumDriver driver;
    public ModalRegistrodeHuellas()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@value,\"Registro de huellas\")]")
    public MobileElement lblTituloRegistrodeHuellas;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@value,\"No hemos detectado ninguna huella registrada en tu teléfono. Configura una para poder ingresar a la aplicación de forma más rápida.\")]")
    //@iOSFindBy(id = "HERALDO, has autorizado el uso de tus huellas para el ingreso a la app Bci.")
    public MobileElement lblDescripcionRegistrodeHuellas;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@label,\"CONFIGURAR HUELLAS\")]")
    //@iOSFindBy(id = "HERALDO, has autorizado el uso de tus huellas para el ingreso a la app Bci.")
    public MobileElement btnAdministrarhuelas;

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "OMITIR")
    public MobileElement btnOmitir;


    //===========METODOS==========================================================================================================================

    public void validarObjetosModalRegistroDeHuella (){

        compararTextoConReporte(lblTituloRegistrodeHuellas, "Registro de huellas");
        compararTextoConReporte(lblDescripcionRegistrodeHuellas, "No hemos detectado ninguna huella registrada en tu teléfono. Configura una para poder ingresar a la aplicación de forma más rápida.");
        compararTextoConReporte(btnAdministrarhuelas, "CONFIGURAR HUELLAS");
        compararTextoConReporte(btnOmitir, "OMITIR");
    }

    public void presionarConfigurarHuellas(){
        btnAdministrarhuelas.click();
    }

    public void presionarOmitirModalRegistroHuellas(){
        btnOmitir.click();
    }

    public void presionarConfigurarHuellasSinhuellaRegistrada()
    {
        btnAdministrarhuelas.click();
        //volvemos a la app
        PdfBciReports.addMobilReportImage("presionarConfigurarHuellasSinhuellaRegistrada","Pantalla de Configuración", EstadoPrueba.PASSED, false);
        Reports.addStep( "pantalla de configuración",true, Status.PASSED, false);

        driver.launchApp();
    }
}
