package pages.Unboxing;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;

import static util.MetodosGenericos.compararTextoConReporte;

public class AutorizarFaceId
{
    AppiumDriver driver;

    public AutorizarFaceId()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Inicia sesión con tu rostro")
    public MobileElement lblTitulo;

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Ingresa de manera más rápida y segura usando el reconocimiento facial configurado en tu teléfono.")
    public MobileElement lblDescripcion;

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Omitir")
    public MobileElement btnOmitir;

    //Cambiar id
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "AUTORIZAR FACEID")
    public MobileElement btnAutorizarFaceID;


    //===========METODOS==========================================================================================================================

    @Step ("Validar Objetos en pantalla de Autorizar FaceID.")
    public void validarObjetosAutorizarFaceID()
    {
        compararTextoConReporte(btnOmitir, "Omitir");
        compararTextoConReporte(lblTitulo, "Inicia sesión con tu rostro");
        compararTextoConReporte(lblDescripcion, "Ingresa de manera más rápida y segura usando el reconocimiento facial configurado en tu teléfono.");
        compararTextoConReporte(btnAutorizarFaceID, "AUTORIZAR FACEID");
    }

    @Step ("Presiona el Boton Autorizar FaceID")
    public void presionarAutorizarFaceID(){ btnAutorizarFaceID.click(); }

    @Step ("Presiona el Boton Omitir.   ")
    public void presionarOmitir () { btnOmitir.click(); }


}
