package pages.Unboxing;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;


import static util.MetodosGenericos.compararTextoConReporte;
import static util.MetodosGenericos.compararTextoIncompletoConReporte;

public class MensajeBienvenida
{
    AppiumDriver driver;
    public MensajeBienvenida()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "¡Bienvenido!")
    public MobileElement lblTituloBienvenida;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@name,\", has autorizado el uso de tus huellas para el ingreso a la app Bci.\")]")
    //@iOSFindBy(id = "HERALDO, has autorizado el uso de tus huellas para el ingreso a la app Bci.")
    public MobileElement lblDescripcionBienvenida;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@name,\", puedes autorizar el uso de tus huellas en cualquier momento desde tu perfil de usuario.\")]")
    public MobileElement lblDescripcionBienvenidaSinAutorizarHuella;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"HERALDO, has autorizado el uso de FaceID para el ingreso a la app Bci.\"]")
    public MobileElement lblDescripcionBienvenidaAutorizarFaceID;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@name,\", puedes autorizar el uso de FaceId en cualquier momento desde tu perfil de usuario.\")]")
    public MobileElement lblDescripcionBienvenidaSinAutorizarFaceID;

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "IR A MI CUENTA")
    public MobileElement btnBienvenida;


    //BOTON DE BIENVENIDA ANTIGUA.
    @AndroidFindBy(id = "")
    @iOSFindBy(id = "Comenzar")
    public MobileElement btnComenzar;


    //===========METODOS==========================================================================================================================

    public void validarObjetosBienvenida()
    {
        compararTextoConReporte(lblTituloBienvenida, "¡Bienvenido!");
        compararTextoIncompletoConReporte(lblDescripcionBienvenida, ", has autorizado el uso de tus huellas para el ingreso a la app Bci.");
        compararTextoConReporte(btnBienvenida, "IR A MI CUENTA");
    }

    public void validarObjetosBienvenidaAutorizarFaceID()
    {
        compararTextoConReporte(lblTituloBienvenida, "¡Bienvenido!");
        //compararTextoIncompletoConReporte(lblDescripcionBienvenidaAutorizarFaceID, ", has autorizado el uso de FaceID para el ingreso a la app Bci.");
        compararTextoConReporte(btnBienvenida, "IR A MI CUENTA");
    }

    public void validarObjetosBienvenidaDespuesDePresionarOmitir()
    {
        compararTextoConReporte(lblTituloBienvenida, "¡Bienvenido!");
        compararTextoIncompletoConReporte(lblDescripcionBienvenidaSinAutorizarHuella, ", puedes autorizar el uso de tus huellas en cualquier momento desde tu perfil de usuario.");
        compararTextoConReporte(btnBienvenida, "IR A MI CUENTA");
    }

    public void validarObjetosBienvenidaDespuesDePresionarOmitirFaceID()
    {
        compararTextoConReporte(lblTituloBienvenida, "¡Bienvenido!");
        compararTextoIncompletoConReporte(lblDescripcionBienvenidaSinAutorizarFaceID, ", puedes autorizar el uso de FaceId en cualquier momento desde tu perfil de usuario.");
        compararTextoConReporte(btnBienvenida, "IR A MI CUENTA");
    }

    public void validarObjetosBienvenidaSinhuella()
    {
        compararTextoConReporte(lblTituloBienvenida, "¡Bienvenido!");
        compararTextoConReporte(lblDescripcionBienvenidaSinAutorizarHuella, ", puedes autorizar el uso de tus huellas en cualquier momento desde tu perfil de usuario.");
        compararTextoConReporte(btnBienvenida, "IR A MI CUENTA");
    }

    public void presionarIraMiCuenta(){ btnBienvenida.click(); }

    public void presionarComenzar(){ btnComenzar.click(); }
}
