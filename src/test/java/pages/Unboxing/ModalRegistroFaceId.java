package pages.Unboxing;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.compararTextoConReporte;

public class ModalRegistroFaceId
{
    AppiumDriver driver;

    public ModalRegistroFaceId()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@value,\"Registro de FaceId\")]")
    public MobileElement lblTituloRegistroDeFaceID;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@value,\"No hemos detectado ningún FaceId registrado en tu teléfono. Configura uno para poder ingresar a la aplicación de forma más rápida.\")]")
    public MobileElement lblDescripcionRegistroDeFaceID;

    @AndroidFindBy(id = "")
    @iOSFindBy(xpath = "//*[contains(@label,\"CONFIGURAR FACEID\")]")
    public MobileElement btnAdministrarFaceID;

    @AndroidFindBy(id = "")
    @iOSFindBy(id = "OMITIR")
    public MobileElement btnOmitir;

    //===========METODOS==========================================================================================================================

    public void validarObjetosModalRegistroDeHuella ()
    {
        compararTextoConReporte(lblTituloRegistroDeFaceID, "Registro de FaceId");
        compararTextoConReporte(lblDescripcionRegistroDeFaceID, "No hemos detectado ningún FaceId registrado en tu teléfono. Configura uno para poder ingresar a la aplicación de forma más rápida.");
        compararTextoConReporte(btnAdministrarFaceID, "CONFIGURAR FACEID");
        compararTextoConReporte(btnOmitir, "OMITIR");
    }

    public void presionarOmitirModalRegistroFaceID(){
        btnOmitir.click();
    }

    public void presionarConfigurarFaceIDSinFaceIDRegistrada()
    {
        btnAdministrarFaceID.click();

        PdfBciReports.addMobilReportImage("presionarConfigurarFaceIDSinFaceIDRegistrada","Pantalla de Configuración", EstadoPrueba.PASSED, false);
        Reports.addStep( "Pantalla de Configuración",true, Status.PASSED, false);

        //VOLVER A LA APP.
        //driver.launchApp();
    }
}
