package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import org.openqa.selenium.support.PageFactory;
import utils.MetodosGenericos;

import static util.MetodosGenericos.compararTextoConReporte;
import static utils.MetodosGenericos.visualizarObjeto;


public class MenuLateral
{
    AppiumDriver driver;

    public MenuLateral()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    //BOTON PARA CERRAR SESSION
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/db_tv_nav_close_session")
    @iOSFindBy(id = "MenuViewController.logout")
    public MobileElement btnCerrarSession;

    //CONFIRMAR CERRAR SESSION
    @AndroidFindBy(id = "android:id/button1")
    @iOSFindBy(accessibility = "Confirmar")
    public MobileElement btnConfirmarCerrarSession;

    //DESACTIVAR HUELLA
    //@AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/db_tv_nav_close_session")
    @iOSFindBy(id = "Desactivar huella")
    public MobileElement btnDesactivarHuella;

    //BOTON ACTIVAR
    //@AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/db_tv_nav_close_session")
    @iOSFindBy(id = "Autorizar huella")
    public MobileElement btnActivarHuella;

    //===========METODOS==========================================================================================================================


    @Step("Se hace Click en Desactivar Huella.")
    public void clickDesactivarHuella() throws InterruptedException
    {
        boolean existeBtnDesactivarHuella = visualizarObjeto(btnDesactivarHuella, 10);

        if(existeBtnDesactivarHuella == true)
        {
            System.out.println("Se visualiza Botón Desactivar Huella en Menu Lateral.");
            PdfBciReports.addMobilReportImage("clickDesactivarHuella","Se visualiza Botón Desactivar Huella en Menu Lateral.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Botón Desactivar Huella en Menu Lateral.",true, Status.PASSED,false);

            btnDesactivarHuella.click();

        }else{
            PdfBciReports.addMobilReportImage("clickDesactivarHuella","No se visualiza Botón Desactivar Huella en Menu Lateral.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza Botón Desactivar Huella en Menu Lateral.",true, Status.FAILED,true);
        }
    }

    @Step("Se hace Click en Autorizar Huella.")
    public void clickActivarHuella() throws InterruptedException
    {
        boolean existeBtnActivarHuella = visualizarObjeto(btnActivarHuella, 10);

        if(existeBtnActivarHuella == true)
        {
            System.out.println("Se visualiza Botón Activar Huella en Menu Lateral.");
            PdfBciReports.addMobilReportImage("clickActivarHuella","Se visualiza Botón Activar Huella en Menu Lateral.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Botón Activar Huella en Menu Lateral.",true, Status.PASSED,false);

            btnActivarHuella.click();

        }else{
            PdfBciReports.addMobilReportImage("clickActivarHuella","No se visualiza Botón Activar Huella en Menu Lateral.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza Botón Activar Huella en Menu Lateral.",true, Status.FAILED,true);
        }
    }

    @Step("Se hace click en cerrar session y confirma la operacion.")
    //METODO PARA CERRAR  CESSION DESDE EL MENU LATERAL Y CONFIRMAR
    public void clickCerrarSession() throws InterruptedException {
        boolean existeCerrarSession = MetodosGenericos.visualizarObjeto(btnCerrarSession, 5);
        if(existeCerrarSession == true){
            System.out.println("Aparece mensaje cerrar session.");
            btnCerrarSession.click();
            System.out.println("Se cierra session.");
            Thread.sleep(3000);
            System.out.println("Aparece mensaje de confirmación.");
            boolean existeConfirmarCerarSession = MetodosGenericos.visualizarObjeto(btnConfirmarCerrarSession, 3);
            if(existeConfirmarCerarSession){
                btnConfirmarCerrarSession.click();
                System.out.println("Se confirma.");
            }
        }else {
            PdfBciReports.addMobilReportImage("clickCerrarSession","No se visualiza botón para cerrar session.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza botón para cerrar session.",true, Status.FAILED,true);
        }
    }
}
