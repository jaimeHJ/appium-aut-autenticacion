package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;

import java.util.List;



public class SelectorPagosyRecargas
{
    public SelectorPagosyRecargas()
    {
        PageFactory.initElements(new AppiumFieldDecorator(DriverContext.getDriver()),this);
    }


    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID+":id/tv_opcion_name")
    @iOSFindBy(id = "aca accesibility id o name")
    public List<MobileElement> subTab;
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID+":id/tab_title")
    @iOSFindBy(id = "aca accesibility id o name")
    public List<MobileElement> tabTitle;


    //===========METODOS==========================================================================================================================

    @Step
    public void seleccionarPagos()
    {
        DriverContext.setDriverTimeout(10);
        tabTitle.get(4).click();
        subTab.get(0).click();
    }
}
