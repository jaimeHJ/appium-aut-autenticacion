package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;


import static util.MetodosGenericos.compararTextoConReporte;
import static util.MetodosGenericos.isChecked;
import static utils.MetodosGenericos.reporteObjetoDesplegado;
import static utils.MetodosGenericos.visualizarObjeto;
import static utils.Utils.isEnabled;

public class TerminosYCondiciones
{
    AppiumDriver driver;

    public TerminosYCondiciones()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/accept_terms_checkbox")
    @iOSFindBy(id = "AppTermsConditionsViewController.checkButton")
    public MobileElement checkboxTYC;

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/btn_accept")
    @iOSFindBy(id = "AppTermsConditionsViewController.acceptButton")
    public MobileElement btnAceptarTYC;

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tyc_title")
    @iOSFindBy(id = "AppTermsConditionsViewController.titleLabel")
    public MobileElement tituloModalTYC;

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tyc_last_line")
    @iOSFindBy(id = "AppTermsConditionsViewController.tcTextView")
    public MobileElement descripcionTYC;


    //===========METODOS==========================================================================================================================

    @Step("Validar apertura del modal Terminos y condiciones.")
    public void validarInicio(){
        reporteObjetoDesplegado(visualizarObjeto(tituloModalTYC, 30), "Modal Término y condiciones", "Modal de Termino y condiciones", false);
    }

    @Step("Validar Objetos del modal Terminos y condiciones.")
    public void validarObjetosModalTYC(){
        System.out.println("Validar objetos del modal de término y condiciones");
        compararTextoConReporte(tituloModalTYC, "Términos y condiciones");
        reporteObjetoDesplegado(visualizarObjeto(checkboxTYC, 10),"Checkbox Aceptar Terminos y condiciones", "Modal de Terminos y condiciones.", false);
        reporteObjetoDesplegado(!isChecked(checkboxTYC), "Checkbox deschecked", "Modal de Terminos y condiciones", false);
        compararTextoConReporte(descripcionTYC, "Acepto los Términos y condiciones,\n" +
                "las Políticas de Privacidad de Bci y autorizo el tratamiento de mis datos personales en Chile o en el extranjero.");
        compararTextoConReporte(btnAceptarTYC, "ACEPTAR");
        reporteObjetoDesplegado(!isEnabled(btnAceptarTYC), "Botón Aceptar Deshabilitado", "Modal de Terminos y condiciones", false);
    }

    @Step("Aceptar términos del modal Terminos y condiciones.")
    public void aceptarTerminosYCondiciones()
    {
        System.out.println("Aceptar término y condiciones");
        checkboxTYC.click();
        reporteObjetoDesplegado(isChecked(checkboxTYC), "Checkbox checked", "Modal de Terminos y condiciones", false);
        reporteObjetoDesplegado(isEnabled(btnAceptarTYC), "Botón Aceptar Habilitado", "Modal de Terminos y condiciones", false);
        btnAceptarTYC.click();
        PdfBciReports.addMobilReportImage("aceptarTerminosYCondiciones", "Se aceptan terminos y condiciones", EstadoPrueba.PASSED, false);
        Reports.addStep("Se aceptan terminos y condiciones",true, Status.PASSED,false);
    }

    @Step("Aceptar términos del modal Terminos y condiciones Sin Validar.")
    public void  aceptarTyCSinValidar()
    {
        System.out.println("Aceptar término y condiciones");
        checkboxTYC.click();
        //reporteObjetoDesplegado(isChecked(checkboxTYC), "Checkbox checked", "Modal de Terminos y condiciones", false);
        //reporteObjetoDesplegado(isEnabled(btnAceptarTYC), "Botón Aceptar Habilitado", "Modal de Terminos y condiciones", false);
        btnAceptarTYC.click();
        PdfBciReports.addMobilReportImage("aceptarTyCSinValidar", "Se aceptan terminos y condiciones", EstadoPrueba.PASSED, false);
        Reports.addStep("Se aceptan terminos y condiciones",true, Status.PASSED,false);
    }
}
