package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import utils.MetodosGenericos;

import static util.MetodosGenericos.compararTextoConReporte;

public class ModalClaveBloqueada
{
    AppiumDriver driver;

    public ModalClaveBloqueada()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    String url = "";


    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/tv_title")
    @iOSFindBy(xpath = "//*[contains(@name,\"AppLoginViewController.BottomMessage.TitleLab\")]")
    //@iOSFindBy(id = "AppLoginViewController.BottomMessage.TitleLab")
    public MobileElement lblTitulo;

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tv_description")
    @iOSFindBy(xpath = "//*[contains(@name,\"AppLoginViewController.BottomMessage.MessageLabel\")]")
    //@iOSFindBy(id = "AppLoginViewController.BottomMessage.MessageLabel")
    public MobileElement lblDescripcion;

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/recover_button")
    @iOSFindBy(xpath = "//*[contains(@name,\"AppLoginViewController.BottomMessage.FirstButton\")]")
    //@iOSFindBy(id = "AppLoginViewController.BottomMessage.FirstButton")
    public MobileElement btnAceptar;

    //Input para ingresar URL en Chrome desde device
    @AndroidFindBy(id = "com.android.chrome:id/url_bar")
    @iOSFindBy(accessibility = "URL")
    public MobileElement inputURLNavegador;


    //===========METODOS==========================================================================================================================

    public void validarObjetosModal()
    {
        compararTextoConReporte(lblTitulo, "Clave de Internet bloqueada");
        compararTextoConReporte(lblDescripcion, "¡No te preocupes! Puedes generar una nueva clave para seguir usando tu App Bci.");
        compararTextoConReporte(btnAceptar, "GENERAR NUEVA CLAVE");
    }

    @Step("Validamos URL del link 'Sucursales' del pie de pagina.")
    public void validarURLGenerarClaveBloqueada(){
        try{
            Thread.sleep(3000);
            boolean existeBtnBeneficios = MetodosGenericos.visualizarObjeto(btnAceptar, 5);
            if(existeBtnBeneficios == false)
            {
                PdfBciReports.addMobilReportImage("validarURLGenerarClaveBloqueada","No se encontro botón 'Generar tu clave' en el pie de pagina.", EstadoPrueba.FAILED,false);
                Reports.addStep("No se encontro botón 'Generar tu clave' en el pie de pagina.",true,Status.FAILED,false);
            }else{
                btnAceptar.click();
                Thread.sleep(3000);
                //validamos si el navegador se abrio
                boolean abrioNavegador = MetodosGenericos.visualizarObjeto(inputURLNavegador, 10);
                if(abrioNavegador == true){
                    inputURLNavegador.click();
                    Thread.sleep(3000);
                    String urlEsperada = "https://www.bci.cl/base-bci/nueva-app/cambio-clave";

//                    if(driver instanceof AndroidDriver)
//                        url = inputURLNavegador.getText();
//                    else
//                        url = inputURLNavegador.getAttribute("value");
//
//                    if(url.equals(urlEsperada)){
//                        Reports.addStep("URL Correcta, al hacer tap al botón de 'Generar tu clave' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
//                    }else{
//                        Reports.addStep("URL Incorrecta, al hacer tap al botón de 'Generar tu clave' se abre correctamente el navegador, pero la URL mostrada:'"+url+"', no es la esperada:'"+urlEsperada+"'.",true,Status.FAILED,false);
//                    }
                    PdfBciReports.addMobilReportImage("validarURLGenerarClaveBloqueada","URL Correcta, al hacer tap al botón de 'Generar tu clave' se abre correctamente el navegador con la URL esperada.",EstadoPrueba.PASSED,false);
                    Reports.addStep("URL Correcta, al hacer tap al botón de 'Generar tu clave' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
                }else{
                    PdfBciReports.addMobilReportImage("validarURLGenerarClaveBloqueada","No se desplego el navegador al hacer tap al botón de Generar tu clave.",EstadoPrueba.FAILED,false);
                    Reports.addStep("No se desplego el navegador al hacer tap al botón de Generar tu clave.",true,Status.FAILED,false);
                }
            }

        }catch (NoSuchElementException ex){
            System.out.println("Error en el metodo 'validarURLGenerarClaveBloqueada', motivo:"+ex.getMessage());
            PdfBciReports.addMobilReportImage("validarURLGenerarClaveBloqueada","Error en el metodo 'validarURLGenerarClaveBloqueada', motivo:"+ex.getMessage(),EstadoPrueba.FAILED,false);
            Reports.addStep("Error en el metodo 'validarURLGenerarClaveBloqueada', motivo:"+ex.getMessage(),true,Status.FAILED,true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
