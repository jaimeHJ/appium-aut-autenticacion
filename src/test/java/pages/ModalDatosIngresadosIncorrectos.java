package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.Reports;
import utils.MetodosGenericos;

import static util.MetodosGenericos.compararTextoConReporte;

public class ModalDatosIngresadosIncorrectos
{
    AppiumDriver driver;

    public ModalDatosIngresadosIncorrectos()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/tv_title")
    @iOSFindBy(id = "AppLoginViewController.BottomMessage.TitleLab")
    public MobileElement lblTitulo;

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tv_description")
    @iOSFindBy(id = "AppLoginViewController.BottomMessage.MessageLabel")
    public MobileElement lblDescripcion;

    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/cb_close")
    @iOSFindBy(id = "AppLoginViewController.BottomMessage.FirstButton")
    public MobileElement btnAceptar;


    //===========METODOS==========================================================================================================================

    public void validarObjetosModal()
    {
        compararTextoConReporte(lblTitulo, "Datos ingresados incorrectos.");
        compararTextoConReporte(lblDescripcion, "Por favor revisa tus datos e ingrésalos nuevamente.");
        compararTextoConReporte(btnAceptar, "ACEPTAR");
    }

    public void presionarAceptar(){
        btnAceptar.click();
    }
}
