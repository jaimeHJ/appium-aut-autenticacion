package pages.AvisoDeViaje;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.compararTextoIncompletoConReporte;
import static utils.MetodosGenericos.visualizarObjeto;

public class Calendario
{
    AppiumDriver driver;

    public Calendario()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "DatePicker.UIBarButtonItem.Seleccionar")
    public MobileElement btnSeleccionar;

    @iOSFindBy(xpath = "//*[@name=\"DatePicker.UIDatePicker.date\"]/XCUIElementTypeOther/XCUIElementTypePickerWheel[1]")
    public MobileElement calendarioDia;

    @iOSFindBy(xpath = "//*[@name=\"DatePicker.UIDatePicker.date\"]/XCUIElementTypeOther/XCUIElementTypePickerWheel[2]")
    public MobileElement calendarioMes;

    @iOSFindBy(xpath = "//*[@name=\"DatePicker.UIDatePicker.date\"]/XCUIElementTypeOther/XCUIElementTypePickerWheel[3]")
    public MobileElement calendarioAno;


    //===========METODOS==========================================================================================================================

    public void fijarDia (String Dia)
    {
        calendarioDia.setValue(Dia);
    }

    public void fijarMes (String Mes)
    {
        calendarioMes.setValue(Mes);
    }

    public void fijarAno (String Ano)
    {
        calendarioAno.setValue(Ano);
    }

    @Step ("Validar Modal Calendario.")
    public void validarModalCalendario ()
    {
        boolean existeBtnSeleccionar = visualizarObjeto(btnSeleccionar, 10);

        if(existeBtnSeleccionar == true)
        {
            PdfBciReports.addMobilReportImage("clickSeleccionar","Se visualiza Botón Seleccionar en Modal Calendario.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Botón Seleccionar en Modal Calendario.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("clickSeleccionar","No se visualiza Botón Seleccionar en Modal Calendario.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza Botón Seleccionar en Modal Calendario.",true, Status.FAILED,true);
        }
    }

    @Step ("Hacer Click en Botón Seleccionar en Modal Calendario.")
    public void clickSeleccionar ()
    {
        btnSeleccionar.click();
    }

}
