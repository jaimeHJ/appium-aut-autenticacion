package pages.AvisoDeViaje;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.compararTextoConReporte;
import static util.MetodosGenericos.compararTextoIncompletoConReporte;
import static utils.MetodosGenericos.visualizarObjeto;

public class InformarUsoEnElExtranjero
{
    AppiumDriver driver;

    public InformarUsoEnElExtranjero()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "icon_back_black_TC")
    public MobileElement btnAtras;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.Title")
    public MobileElement textoTitulo;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.Message")
    public MobileElement textoSubTitulo;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.StartDate.Title")
    public MobileElement textoCambiarDesde;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"ImageSelectableButton.iconView\"])[1]")
    public MobileElement iconoCambiarDesde;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"SelectableButton.valueLabel\"])[1]")
    public MobileElement textoDdmmaaaaDesde;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"SelectableButton.changeLabel\"])[1]")
    public MobileElement btnCambiarDesde;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.EndDate")
    public MobileElement textoCambiarHasta;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"ImageSelectableButton.iconView\"])[2]")
    public MobileElement iconoCambiarHasta;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"SelectableButton.valueLabel\"])[2]")
    public MobileElement textoDdmmaaaaHasta;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"SelectableButton.changeLabel\"])[2]")
    public MobileElement btnCambiarHasta;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "AvisoViajeFormulario.contactPhone")
    public MobileElement inputNumContacto;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.CountryPickerLabel")
    public MobileElement textoDondeUsarasTuTarjeta;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UIImageView.Add")
    public MobileElement iconoAgregarPais;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.AddCountries.Title")
    public MobileElement btnAgregarPais;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.Button.InformCardUso")
    public MobileElement btnInformarUsoDeTarjeta;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"SelectableButton.valueLabel\"])[1]")
    public MobileElement textoFechaDesdeSeleccionada;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"SelectableButton.valueLabel\"])[2]")
    public MobileElement textoFechaHastaSeleccionada;


    //===========METODOS==========================================================================================================================

    @Step ("Validar Vista Informar uso en el extranjero")
    public void validarVistaInformarUsoEnElExtranjero ()
    {
        boolean existeTitulo = visualizarObjeto(textoTitulo, 10);
        boolean existeIconoCambiarHasta = visualizarObjeto(iconoCambiarHasta, 10);
        boolean existeIconoCambiarDesde = visualizarObjeto(iconoCambiarDesde, 10);
        boolean existeIconoAgregarPais = visualizarObjeto(iconoAgregarPais, 10);

        if(existeIconoCambiarDesde == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","Se encontró Icono ¿Desde cuándo la usarás? en vista Informar uso en el extranjero.",EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró Icono ¿Desde cuándo la usarás? en vista Informar uso en el extranjero.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","No se encontró Icono ¿Desde cuándo la usarás? en vista Informar uso en el extranjero.",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró Icono ¿Desde cuándo la usarás? en vista Informar uso en el extranjero.",true, Status.FAILED,true);
        }

        if(existeIconoCambiarHasta == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","Se encontró Icono ¿Hasta cuándo la usarás? en vista Informar uso en el extranjero.",EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró Icono ¿Hasta cuándo la usarás? en vista Informar uso en el extranjero.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","No se encontró Icono ¿Hasta cuándo la usarás? en vista Informar uso en el extranjero.",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró Icono ¿Hasta cuándo la usarás? en vista Informar uso en el extranjero.",true, Status.FAILED,true);
        }

        if(existeIconoAgregarPais == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","Se encontró Icono Elige uno o más países en vista Informar uso en el extranjero.",EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró Icono Elige uno o más países en vista Informar uso en el extranjero.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró Icono Elige uno o más países en vista Informar uso en el extranjero.",true, Status.FAILED,true);
        }

        if(existeTitulo == true)
        {
            System.out.println("Se encontró Titulo en vista Informar uso en el extranjero.");
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","Se encontró Titulo en vista Informar uso en el extranjero.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró Titulo en vista Informar uso en el extranjero.",true, Status.PASSED,false);

            ListaDeTarjetas ListaDeTarjetas = new ListaDeTarjetas();

            compararTextoIncompletoConReporte(textoSubTitulo,ListaDeTarjetas.NomTarjeta);
            compararTextoConReporte(textoCambiarDesde, "¿Desde cuándo la usarás?");
            compararTextoConReporte(textoDdmmaaaaDesde,"dd/mm/aaaa");
            compararTextoConReporte(textoCambiarHasta, "¿Hasta cuándo la usarás?");
            compararTextoConReporte(textoDdmmaaaaHasta,"dd/mm/aaaa");
            compararTextoConReporte(textoDondeUsarasTuTarjeta,"¿Dónde usarás tu tarjeta?");
            compararTextoConReporte(btnAgregarPais,"Elige uno o más países");
            compararTextoConReporte(btnInformarUsoDeTarjeta,"INFORMAR USO DE TARJETA");

        }else{
            PdfBciReports.addMobilReportImage("validarVistaInformarUsoEnElExtranjero","No se encontró Titulo en vista Informar uso en el extranjero.",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró Titulo en vista Informar uso en el extranjero.",true, Status.FAILED,true);
        }
    }

    @Step ("Hacer Click en Boton Cambiar del Calendario ¿Desde cuando la usaras?")
    public void clickBotonCambiarDesde ()
    {
        btnCambiarDesde.click();
        System.out.println("Se selecciona Fecha Desde.");
    }

    @Step ("Hacer Click en Boton Cambiar del Calendario ¿Hasta cuando la usaras?")
    public void clickBotonCambiarHasta ()
    {
        btnCambiarHasta.click();
        System.out.println("Se selecciona Fecha Hasta.");
    }

    @Step ("Escribir Numero de Contacto.")
    public void escribirNumeroContacto (String Numero)
    {
        inputNumContacto.sendKeys(Numero);
        System.out.println("Se ecribe Numero de Contacto.");
    }

    @Step ("Hacer Click en Texto Elige uno o mas paises")
    public void clickEligeUnooMasPaises ()
    {
        btnAgregarPais.click();
        System.out.println("Se Clickea en Texto Elige uno o más paises.");
    }

    @Step ("Hacer Click en Boton Informar uso de Tarjeta.")
    public void clickBotonInformarUsoDeTarjeta ()
    {
        btnInformarUsoDeTarjeta.click();
        System.out.println("Se Clickea en Boton Informar uso de Tarjeta.");
    }
}