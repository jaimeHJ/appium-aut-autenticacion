package pages.AvisoDeViaje;

import constants.SwipeDirection;
import driver.DriverContext;
import gestures.Gestures;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.*;
import static utils.MetodosGenericos.visualizarObjeto;

public class Tarjetas
{
    AppiumDriver driver;

    public Tarjetas()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //===========OBJETOS==========================================================================================================================

    //@AndroidFindBy(className = "")
    //@iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[6]")
    @iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[5]")
    public MobileElement btnPagoDeTarjetas;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[1]")
    public MobileElement btnSolicitarAvance;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[2]")
    public MobileElement btnProgramasDeBeneficios;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[3]")
    public MobileElement btnBloqueoDeTarjetas;

    /*//@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[4]")
    public MobileElement btnTarjetaVirtual;*/

    //@AndroidFindBy(className = "")
    //@iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[5]")
    @iOSFindBy(xpath = "(//*[@name=\"TarjetasDashboard.ActionButton.TitleLab\"])[4]")
    public MobileElement btnInformar;


    //===========METODOS==========================================================================================================================

    @Step ("Validar Objetos en Menu Tarjetas.")
    public void validarMenuTarjetas ()
    {
        boolean existeBtnPagoTarjetas = visualizarObjeto(btnPagoDeTarjetas, 10);

        if(existeBtnPagoTarjetas == true)
        {
            System.out.println("Se abrio Menu Tarjetas correctamente.");
            PdfBciReports.addMobilReportImage("validarMenuTarjetas","Se abrio Menu Tarjetas correctamente.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se abrio Menu Tarjetas correctamente.",true, Status.PASSED,false);

            compararTextoIncompletoConReporte(btnSolicitarAvance, "Solicitar Avance");
            compararTextoIncompletoConReporte(btnProgramasDeBeneficios, "Programas de Beneficios");
            Gestures.scrollMovimientos();
            compararTextoIncompletoConReporte(btnBloqueoDeTarjetas, "Bloqueo de tarjetas");
            //compararTextoIncompletoConReporte(btnTarjetaVirtual, "Tarjeta Virtual");
            compararTextoIncompletoConReporte(btnInformar, "Informar uso en el extranjero");

        }else{
            PdfBciReports.addMobilReportImage("validarMenuTarjetas","No se visualiza botón para abrir menu lateral.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza botón para abrir menu lateral.",true, Status.FAILED,true);
        }
    }

    @Step ("Hacer Click en Boton Informar uso en el Extranjero.")
    public void ClickBtnInformar ()
    {
        btnInformar.click();
        System.out.println("Se presionó Informar Uso en el Extranjero.");

    }

}
