package pages.AvisoDeViaje;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.compararTextoConReporte;
import static utils.MetodosGenericos.visualizarObjeto;

public class ListaPaises
{
    AppiumDriver driver;

    public ListaPaises()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //===========OBJETOS==========================================================================================================================

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "CerrarCuentas")
    public MobileElement btnX;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "MultiSelector.title")
    public MobileElement textoTitulo;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "Busca el país por su nombre")
    public MobileElement inputBuscador;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "MultiSelector.SelectButton")
    public MobileElement btnSeleccionar;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "//*[@name=\"checkBoxUnchecked\"]")
    //@iOSFindBy(id = "checkBoxUnchecked")
    public MobileElement chckPrimerUnchecked;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "//*[@name=\"checkBoxChecked\"]")
    //@iOSFindBy(id = "checkBoxChecked")
    public MobileElement chckPrimerChecked;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "Selector.label")
    public MobileElement labelPaisBuscado;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "Cancelar")
    public MobileElement btnCancelar;


    //===========METODOS==========================================================================================================================

    @Step ("Validar Vista Lista de Paises.")
    public void validarVistaListaPaises ()
    {
        boolean existetextoTitulo = visualizarObjeto(textoTitulo, 10);
        boolean existeBtnX = visualizarObjeto(btnX, 10);
        boolean existeInputBuscador = visualizarObjeto(inputBuscador, 10);
        boolean existeBtnSeleccionar = visualizarObjeto(btnSeleccionar, 10);


        if(existeBtnX == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","Se encontró el Botón X en Vista Lista de Paises.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró el Botón X en Vista Lista de Paises.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","No se encontró el Botón X en Vista Lista de Paises.",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró el Botón X en Vista Lista de Paises.",true, Status.FAILED,true);
        }

        if(existeInputBuscador == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","Se encontró el Buscador de Países en Vista Lista de Paises.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró el Buscador de Países en Vista Lista de Paises.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","No se encontró el Buscador de Países en Vista Lista de Paises.",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró el Buscador de Países en Vista Lista de Paises.",true, Status.FAILED,true);
        }

        if(existeBtnSeleccionar == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","Se encontró el Botón Seleccionar en Vista Lista de Paises.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró el Botón Seleccionar en Vista Lista de Paises.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","No se encontró el Botón Seleccionar en Vista Lista de Paises.",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró el Botón Seleccionar en Vista Lista de Paises.",true, Status.FAILED,true);
        }

        if(existetextoTitulo == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","Se encontró el Titulo en Vista Lista de Paises.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se encontró el Titulo en Vista Lista de Paises.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaListaPaises","No se encontró el Titulo en Vista Lista de Paises.",EstadoPrueba.FAILED,true);
            Reports.addStep("No se encontró el Titulo en Vista Lista de Paises.",true, Status.FAILED,true);
        }
    }

    @Step ("Escribir un Pais en la barra de busqueda.")
    public void escribirPais (String Pais)
    {
        inputBuscador.clear();
        inputBuscador.sendKeys(Pais);
    }

    @Step ("Hacer Click en el Boton Cancelar.")
    public void clickCancelar ()
    {
        btnCancelar.click();
    }

    @Step ("Hacer Click en el Boton X.")
    public void clickBotonX ()
    {
        btnX.click();
    }

    @Step ("Seleccionar Pais buscado.")
    public void SeleccionarPais ()
    {
        chckPrimerUnchecked.click();
        System.out.println("Se Selecciona un País.");
    }

    @Step ("Desmarcar un Pais anteriormente seleccionado.")
    public void DesmarcarPais ()
    {
        chckPrimerChecked.click();
        System.out.println("Se Desmarca un País.");
    }

    @Step ("Hacer Click en Boton Seleccionar.")
    public void clickSeleccionar ()
    {
        btnSeleccionar.click();
    }

    @Step ("Click en País Buscado, para Marcar o Desmarcar.")
    public void clickPaisBuscado ()
    {
        labelPaisBuscado.click();
        System.out.println("Se hace Click en País buscado.");
    }

}
