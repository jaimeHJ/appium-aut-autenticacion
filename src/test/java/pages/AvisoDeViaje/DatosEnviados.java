package pages.AvisoDeViaje;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.*;
import static utils.MetodosGenericos.visualizarObjeto;

public class DatosEnviados
{
    AppiumDriver driver;

    public DatosEnviados()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //===========OBJETOS==========================================================================================================================

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "AvisoViajeComprobanteViewController.Finalizar")
    public MobileElement btnFinalizar;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "AvisoViajeComprobanteViewController.EnvioOtroAviso")
    public MobileElement btnEnviarOtroInforme;


    //===========METODOS==========================================================================================================================

    @Step ("Validar Vista Tus datos fueron enviados")
    public void validarVistaDatosEnviados ()
    {
        boolean existeBtnFinalizar = visualizarObjeto(btnFinalizar, 10);

        if(existeBtnFinalizar == true)
        {
            System.out.println("Se visualiza Botón Finalizar en vista Tus datos fueron enviados.");
            PdfBciReports.addMobilReportImage("validarVistaDatosEnviados","Se visualiza Botón para abrir Menú Lateral.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Botón Finalizar en vista Tus datos fueron enviados.",true, Status.PASSED,false);

            compararTextoConReporte(btnEnviarOtroInforme, "ENVIAR OTRO INFORME");

        }else{
            PdfBciReports.addMobilReportImage("validarVistaDatosEnviados","No se visualiza Botón para abrir Menú Lateral.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza botón para abrir menu lateral.",true, Status.FAILED,true);
        }
    }

    @Step ("Hacer Click en Botón Finalizar.")
    public void clickBotonFinalizar ()
    {
        btnFinalizar.click();
        System.out.println("Se hace Click en Botón Finalizar.");
    }

    @Step ("Hacer Click en Botón Enviar Otro Informe.")
    public void clickBotonEnviarOtroInforme ()
    {
        btnEnviarOtroInforme.click();
        System.out.println("Se hace Click en Botón Enviar Otro Informe.");
    }


}
