package pages.AvisoDeViaje;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static utils.MetodosGenericos.visualizarObjeto;

public class RevisaTusDatos
{
    AppiumDriver driver;

    public RevisaTusDatos()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //===========OBJETOS==========================================================================================================================

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"AvisoViajeConfirmacionViewController.Tarjeta\"])[2]")
    public MobileElement textoTarjetaSeleccionada;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"AvisoViajeConfirmacionViewController.FechaInicial\"])[2]")
    public MobileElement textoFechaDesdeSeleccionada;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"AvisoViajeConfirmacionViewController.FechaFinal\"])[2]")
    public MobileElement textoFechaHastaSeleccionada;

    //@AndroidFindBy(className = "")
    @iOSFindBy(xpath = "(//*[@name=\"AvisoViajeConfirmacionViewController.Paises\"])[2]")
    public MobileElement textoListaPaisesSeleccionados;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "AvisoViajeConfirmacionViewController.ConfirmarDatos")
    public MobileElement btnConfirmarDatos;


    //===========METODOS==========================================================================================================================

    @Step("Validar Modal Tarjeta Bloqueada.")
    public void validarVistaRevisaTusDatos ()
    {
        boolean existeBtnConfirmarDatos = visualizarObjeto(btnConfirmarDatos, 10);

        if(existeBtnConfirmarDatos == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaRevisaTusDatos","Se visualiza Botón Confirmar Datos en Vista Revisa Tus Datos.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Botón Confirmar Datos en Vista Revisa Tus Datos",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaRevisaTusDatos","No se visualiza Botón Confirmar Datos en Vista Revisa Tus Datos.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza Botón Confirmar Datos en Vista Revisa Tus Datos",true, Status.FAILED,true);
        }
    }

    @Step ("Hacer Click en Boton Confirmar Datos.")
    public void clickBtnConfirmarDatos ()
    {
        btnConfirmarDatos.click();
    }
}
