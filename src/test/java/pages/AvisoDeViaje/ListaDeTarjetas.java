package pages.AvisoDeViaje;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static util.MetodosGenericos.compararTextoConReporte;
import static utils.MetodosGenericos.visualizarObjeto;

public class ListaDeTarjetas
{
    public static final java.lang.String BCI_APP_PACKAGE_DEMO = "cl.bci.app.android.avisoviaje.demo";
    AppiumDriver driver;

    public ListaDeTarjetas()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    public String NomTarjeta = "";

    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id = BCI_APP_PACKAGE_DEMO +":id/iv_back_flow")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UIButton.Close")
    public MobileElement btnVolver;

    @AndroidFindBy(id = BCI_APP_PACKAGE_DEMO +":id/tv_avisoviaje_title")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.Title")
    public MobileElement textoTiulo;

    @AndroidFindBy(id = BCI_APP_PACKAGE_DEMO +":id/tv_avisoviaje_subtitle")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.Message")
    public MobileElement textoSubTiulo;

    @AndroidFindBy(id = BCI_APP_PACKAGE_DEMO +":id/iv_avisoviaje_description")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UImageView.Information")
    public MobileElement iconoDescripcion;

    @AndroidFindBy(id = BCI_APP_PACKAGE_DEMO +":id/tv_avisoviaje_description")
    @iOSFindBy(id = "TarjetaCreditoAvisoViaje.UILabel.Information")
    public MobileElement textoDescripcion;

    /*@AndroidFindBy(xpath = "//*[contains(@text,\"N° **** \")]")
    @iOSFindBy(id = "AppLoginViewController.txtRut")
    public MobileElement textoNumeroDeTarjeta;*/


    //===========METODOS==========================================================================================================================

    @Step("Validar objetos de la vista Lista de Tarjetas.")
    public void validarVistaListaDeTarjetas()
    {
        boolean existeBtnVolver = visualizarObjeto(btnVolver, 10);
        boolean existeiconoDescripcion = visualizarObjeto(iconoDescripcion, 10);

        if(existeiconoDescripcion == true)
        {
            PdfBciReports.addMobilReportImage("validarVistaListaDeTarjetas","Se visualiza Icono Descripción en vista Lista de Tarjetas.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Icono Descripción en vista Lista de Tarjetas.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarVistaListaDeTarjetas","No se visualiza Icono Descripción en vista Lista de Tarjetas.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza Icono Descripción en vista Lista de Tarjetas.",true, Status.FAILED,true);
        }

        if(existeBtnVolver == true)
        {
            System.out.println("Se abrió vista Lista de Tarjetas correctamente.");
            PdfBciReports.addMobilReportImage("validarVistaListaDeTarjetas","", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Botón Volver en vista Lista de Tarjetas.",true, Status.PASSED,false);

            compararTextoConReporte(textoTiulo, "Informar uso en el extranjero");
            compararTextoConReporte(textoSubTiulo,"Elige la tarjeta que usarás, completa la información del viaje y finalmente envíanos la notificación.");
            compararTextoConReporte(textoDescripcion, "Te recomendamos que nos informes al menos 24 horas antes de usar tus tarjetas de crédito fuera de Chile.  Así evitarás bloqueos y fraudes.");
        }else{
            PdfBciReports.addMobilReportImage("validarVistaListaDeTarjetas","No se visualiza Botón Volver en vista Lista de Tarjetas.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza Botón Volver en vista Lista de Tarjetas.",true, Status.FAILED,true);
        }
    }

    @Step ("Seleccionar una Tarjeta Bloqueada.")
    public void SeleccionarTarjetaBloqueada ()
    {
        boolean existeTarjeta = true;
        int inc = 1;

        while (existeTarjeta == true)
        {
            if ( !driver.findElements(By.xpath( "(//*[@name=\"AvisoViajeCardCell.cardNumber\"])["+ inc +"]" )).isEmpty() )
            {
                WebElement WE = driver.findElement(By.xpath( "(//*[@name=\"OnOff.CardCell\"])["+ inc +"]" ));

                if ( WE.getText().contains("Bloqueada") || WE.getText().contains("Rechazada") || WE.getText().contains("Suspendida") )
                {
                    NomTarjeta = WE.getText();
                    NomTarjeta = NomTarjeta.replace(", Titular,","");

                    System.out.println("Se encontró Tarjeta Bloqueada.");
                    PdfBciReports.addMobilReportImage("SeleccionarTarjetaBloqueada","Se encontró Tarjeta Bloqueada.", EstadoPrueba.PASSED,false);
                    Reports.addStep("Se encontró Tarjeta Bloqueada.",true, Status.PASSED,false);

                    System.out.println("Se hace Click en Tarjeta Bloqueada.\n");
                    WE.click();
                    break;
                }
            }
            else
            {
                existeTarjeta = false;
            }
        }

        PdfBciReports.addMobilReportImage("SeleccionarTarjetaBloqueada","No se encontró Tarjeta Bloqueada.", EstadoPrueba.FAILED,true);
        Reports.addStep("No se encontró Tarjeta Bloqueada.",true, Status.FAILED,true);
    }

    @Step ("Seleccionar primera Tarjeta Activa.")
    public void SeleccionarTarjetaActiva ()
    {
        boolean existeTarjeta = true;
        int inc = 1;

        while (existeTarjeta == true)
        {
            if ( !driver.findElements(By.xpath( "(//*[@name=\"AvisoViajeCardCell.cardNumber\"])["+ inc +"]" )).isEmpty() )
            {
                WebElement WE = driver.findElement(By.xpath( "(//*[@name=\"OnOff.CardCell\"])["+ inc +"]" ));

                if ( !WE.getText().contains("Bloqueada") || !WE.getText().contains("Rechazada") || !WE.getText().contains("Suspendida") )
                {
                    NomTarjeta = WE.getText();
                    NomTarjeta = NomTarjeta.replace(", Titular,","");

                    System.out.println("Se encontro Tarjeta Activa.");
                    PdfBciReports.addMobilReportImage("SeleccionarTarjetaActiva","Se encontró Tarjeta Activa.", EstadoPrueba.PASSED,false);
                    Reports.addStep("Se encontró Tarjeta Activa.",true, Status.PASSED,false);

                    System.out.println("Se hace Click en Tarjeta Activa.\n");
                    WE.click();
                    break;
                }
            }
            else
            {
                existeTarjeta = false;
            }
        }

        PdfBciReports.addMobilReportImage("SeleccionarTarjetaActiva","No se encontró Tarjeta Activa.", EstadoPrueba.FAILED,true);
        Reports.addStep("No se encontró Tarjeta Activa.",true, Status.FAILED,true);
    }

    @Step ("Contar numero de Tarjetas.")
    public void TotalTarjetas ()
    {
        boolean existeTarjeta = true;
        int inc = 1;

        while (existeTarjeta == true)
        {
            if ( !driver.findElements(By.xpath( "(//*[@name=\"AvisoViajeCardCell.cardNumber\"])["+ inc +"]" )).isEmpty() )
            {
                inc++;
                existeTarjeta = true;
            }
            else
            {
                existeTarjeta = false;
            }
        }

        inc = inc - 1 ;

        System.out.println("El usuario tiene un total de " + inc + " Tarjetas.");
    }
}