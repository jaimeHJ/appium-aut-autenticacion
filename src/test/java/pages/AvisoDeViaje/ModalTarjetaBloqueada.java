package pages.AvisoDeViaje;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import static utils.MetodosGenericos.visualizarObjeto;

public class ModalTarjetaBloqueada
{

    public static final java.lang.String BCI_APP_PACKAGE_DEMO = "cl.bci.app.android.avisoviaje.demo";
    AppiumDriver driver;

    public ModalTarjetaBloqueada()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(id = BCI_APP_PACKAGE_DEMO +":id/tv_avisoviaje_description")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\".BottomMessage.FirstButton\"])[2]")
    public MobileElement btnEntendido;


    //===========METODOS==========================================================================================================================

    @Step("Validar Modal Tarjeta Bloqueada.")
    public void validarModalTarjetaBloqueada ()
    {
        boolean existeBtnEntendido = visualizarObjeto(btnEntendido, 10);

        if(existeBtnEntendido == true)
        {
            PdfBciReports.addMobilReportImage("validarModalTarjetaBloqueada","Se visualiza Botón Entendido en Modal Tarjetas Bloqueadas.", EstadoPrueba.PASSED,false);
            Reports.addStep("Se visualiza Botón Entendido en Modal Tarjetas Bloqueadas.",true, Status.PASSED,false);

        }else{
            PdfBciReports.addMobilReportImage("validarModalTarjetaBloqueada","No se visualiza Botón Entendido en Modal Tarjetas Bloqueadas.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza Botón Entendido en Modal Tarjetas Bloqueadas.",true, Status.FAILED,true);
        }
    }

    @Step("Hacer Click en Botón Entendido.")
    public void clickBotonEntendido ()
    {
        System.out.println("Se hizo Click en Botón Entendido.");
        btnEntendido.click();
    }
}
