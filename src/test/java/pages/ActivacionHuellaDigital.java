package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;

import static util.MetodosGenericos.compararTextoConReporte;
import static util.MetodosGenericos.validaAlias;
import static utils.MetodosGenericos.reporteObjetoDesplegado;
import static utils.MetodosGenericos.visualizarObjeto;

public class ActivacionHuellaDigital
{
    AppiumDriver driver;

    public ActivacionHuellaDigital()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //Cambiar id
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/editText2")
    @iOSFindBy(id = "")
    public MobileElement lblTitulo;

    //Cambiar id
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/textView4")
    @iOSFindBy(id = "")
    public MobileElement btnHabilitarHuella;

    @Step("Validar apertura de la vista Ingresar")
    public void validarInicio(){
        reporteObjetoDesplegado(visualizarObjeto(lblTitulo,10), "Apertura de pantalla Activación huella", "Activación huella", false);
    }

    @Step("Habilitar huella")
    public  void habilitarHuella() {
        btnHabilitarHuella.click();
    }
}
