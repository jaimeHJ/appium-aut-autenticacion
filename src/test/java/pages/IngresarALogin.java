package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import utils.MetodosGenericos;

import static util.MetodosGenericos.*;

public class IngresarALogin extends MetodosGenericos{

    AppiumDriver driver;
    public IngresarALogin() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    String url = "";
    //NO ERES TU
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tv_change_user")
    @iOSFindBy(id = "AppLoginViewController.btnNoEresTu")
    public MobileElement cambiarUsuario;

    //Input para ingresar URL en Chrome desde device
    @AndroidFindBy(id = "com.android.chrome:id/url_bar")
    @iOSFindBy(accessibility = "URL")
    public MobileElement inputURLNavegador;

    //BOTON INGRESAR
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/acb_enter")
    @iOSFindBy(id = "AppLoginViewController.btnIngresar")
    public MobileElement btnIngresar;

    //HOLA ALIAS
    //@AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tv_welcome_user")
    @AndroidFindBy(xpath = "//*[contains(@text,\"Hola \")]")
    @iOSFindBy(xpath = "//*[contains(@name,\"Hola \")]")
    public MobileElement textoAlias;

    //BOTON EMERGENCIA (pie de pagina)
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/emergencias_button")
    @iOSFindBy(accessibility = "Emergencias")
    public MobileElement btnEmergencias;

    //BOTON SUCURSALES (pie de pagina)
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/sucursales_button")
    @iOSFindBy(accessibility = "Sucursales")
    public MobileElement btnSucursales;

    //BOTON CAJEROS (pie de pagina)
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/cajeros_button")
    @iOSFindBy(accessibility = "Cajeros Bci")
    public MobileElement btnCajerosBci;

    //BOTON BENEFICIOS (pie de pagina)
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/descuentos_button")
    @iOSFindBy(accessibility = "Descuentos")
    public MobileElement btnBeneficios;


    //===========METODOS==========================================================================================================================

    @Step("Validar apertura de la vista Ingresar")
    public void validarInicio(){
        reporteObjetoDesplegado(visualizarObjeto(btnIngresar,10), "Pantalla Ingresar a login", "Ingresar a LoginNormal", false);
    }

    @Step("Validar Objetos de la vista Ingresar")
    public  void validarObjetosVistaIngresar(boolean conAlias) throws InterruptedException {
        try{
            //Emitimos un reporte por cada objeto
            if(conAlias) {
                reporteObjetoDesplegado(validaAlias(textoAlias.getText()), "Alias con texto: Hola *", "Ingresar a LoginNormal", false);
                compararTextoConReporte(cambiarUsuario, "¿No eres tú?");
            }
            compararTextoConReporte(btnIngresar, "INGRESAR");
            compararTextoConReporte(btnEmergencias, "Emergencias");
            compararTextoConReporte(btnSucursales, "Sucursales");
            compararTextoConReporte(btnCajerosBci, "Cajeros Bci");

        }catch (NoSuchElementException ex){
            System.out.println("Error en el metodo 'validarObjetosVistaIngresar', motivo:"+ex.getMessage());
            Reports.addStep("Error en el metodo 'validarObjetosVistaIngresar', motivo:"+ex.getMessage(),true,Status.FAILED,true);
        }
    }

    @Step("Validamos URL del link 'Emergencias' del pie de pagina.")
    public void validarURLEmergencias(){
        try{
            boolean existeBtnEmergencias = MetodosGenericos.visualizarObjeto(btnEmergencias, 5);
            if(existeBtnEmergencias == false)
            {
                PdfBciReports.addMobilReportImage("validarURLEmergencias", "No se encontro botón 'Emergencias' en el pie de pagina.", EstadoPrueba.FAILED, false);
                Reports.addStep("No se encontro botón 'Emergencias' en el pie de pagina.",true,Status.FAILED,false);
            }else{
                btnEmergencias.click();
                Thread.sleep(3000);
                System.out.println("Validamos si el navegador abrió");
                boolean abrioNavegador = MetodosGenericos.visualizarObjeto(inputURLNavegador, 10);
                if(abrioNavegador == true){
//                    inputURLNavegador.click();
//                    Thread.sleep(3000);
//                    String urlEsperada = "https://www.bci.cl/personas/servicio-al-cliente";
//
//                    if(driver instanceof AndroidDriver)
//                        url = inputURLNavegador.getText();
//                    else
//                        url = inputURLNavegador.getAttribute("value");
//
//                    System.out.println("Texto del navegador extraído");
//                    if(url.equals(urlEsperada)){
//                        Reports.addStep("URL Correcta, al hacer tap al botón de 'Emergencias' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
//                    }else{
//                        Reports.addStep("URL Incorrecta, al hacer tap al botón de 'Emergencias' se abre correctamente el navegador, pero la URL mostrada:'"+url+"', no es la esperada:'"+urlEsperada+"'.",true,Status.FAILED,false);
//                    }
                    PdfBciReports.addMobilReportImage("validarURLEmergencias", "URL Correcta, al hacer tap al botón de 'Emergencias' se abre correctamente el navegador con la URL esperada.", EstadoPrueba.PASSED, false);
                    Reports.addStep("URL Correcta, al hacer tap al botón de 'Emergencias' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
                    //volvemos a la app
                    driver.launchApp();
                    System.out.println("Estamos en IngresarALogin nuevamente");
                }else{

                    PdfBciReports.addMobilReportImage("validarURLEmergencias", "No se desplego el navegador al hacer tap al botón de emergencias.", EstadoPrueba.FAILED, false);
                    Reports.addStep("No se desplego el navegador al hacer tap al botón de emergencias.",true,Status.FAILED,false);
                }
            }

        }catch (NoSuchElementException ex){
            System.out.println("Error en el metodo 'validarURLEmergencias', motivo:"+ex.getMessage());
            PdfBciReports.addMobilReportImage("validarURLEmergencias", "Error en el metodo 'validarURLEmergencias', motivo:"+ex.getMessage(), EstadoPrueba.FAILED, false);
            Reports.addStep("Error en el metodo 'validarURLEmergencias', motivo:"+ex.getMessage(),true,Status.FAILED,true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step("Validamos URL del link 'Sucursales' del pie de pagina.")
    public void validarURLSucursales(){
        try{
            Thread.sleep(3000);
            boolean existeBtnSucursales = visualizarObjeto(btnSucursales, 5);
            if(existeBtnSucursales == false){
                PdfBciReports.addMobilReportImage("validarURLSucursales", "No se encontro botón 'Sucursales' en el pie de pagina.", EstadoPrueba.FAILED, false);
                Reports.addStep("No se encontro botón 'Sucursales' en el pie de pagina.",true,Status.FAILED,false);
            }else{
                btnSucursales.click();
                Thread.sleep(3000);
                //validamos si el navegador se abrio
                boolean abrioNavegador = MetodosGenericos.visualizarObjeto(inputURLNavegador, 10);
                if(abrioNavegador == true){
//                    inputURLNavegador.click();
//                    Thread.sleep(3000);
//                    String urlEsperada = "https://www.bci.cl/personas/sucursales#sucursales-tab";
//
//                    if(driver instanceof AndroidDriver)
//                        url = inputURLNavegador.getText();
//                    else
//                        url = inputURLNavegador.getAttribute("value");
//
//                    if(url.equals(urlEsperada)){
//                        Reports.addStep("URL Correcta, al hacer tap al botón de 'Sucursales' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
//                    }else{
//                        Reports.addStep("URL Incorrecta, al hacer tap al botón de 'Sucursales' se abre correctamente el navegador, pero la URL mostrada:'"+url+"', no es la esperada:'"+urlEsperada+"'.",true,Status.FAILED,false);
//                    }
                    PdfBciReports.addMobilReportImage("validarURLSucursales", "URL Correcta, al hacer tap al botón de 'Sucursales' se abre correctamente el navegador con la URL esperada.", EstadoPrueba.PASSED, false);
                    Reports.addStep("URL Correcta, al hacer tap al botón de 'Sucursales' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
                    //volvemos a la app
                    driver.navigate().back();
                }else{
                    PdfBciReports.addMobilReportImage("validarURLSucursales", "No se desplego el navegador al hacer tap al botón de sucursales.", EstadoPrueba.FAILED, false);
                    Reports.addStep("No se desplego el navegador al hacer tap al botón de sucursales.",true,Status.FAILED,false);
                }
            }

        }catch (NoSuchElementException ex){
            System.out.println("Error en el metodo 'validarURLSucursales', motivo:"+ex.getMessage());
            PdfBciReports.addMobilReportImage("validarURLSucursales", "Error en el metodo 'validarURLSucursales', motivo:"+ex.getMessage(), EstadoPrueba.FAILED, true);
            Reports.addStep("Error en el metodo 'validarURLSucursales', motivo:"+ex.getMessage(),true,Status.FAILED,true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step("Validamos URL del link 'Sucursales' del pie de pagina.")
    public void validarURLCajerosBci(){
        try{
            Thread.sleep(3000);
            boolean existeBtnCajerosBci = MetodosGenericos.visualizarObjeto(btnCajerosBci, 5);
            if(existeBtnCajerosBci == false)
            {
                PdfBciReports.addMobilReportImage("validarURLCajerosBci", "No se encontro botón 'Cajeros Bci' en el pie de pagina.", EstadoPrueba.FAILED, false);
                Reports.addStep("No se encontro botón 'Cajeros Bci' en el pie de pagina.",true,Status.FAILED,false);
            }else{
                btnCajerosBci.click();
                Thread.sleep(3000);
                //validamos si el navegador se abrio
                boolean abrioNavegador = MetodosGenericos.visualizarObjeto(inputURLNavegador, 10);
                if(abrioNavegador == true){
//                    inputURLNavegador.click();
//                    Thread.sleep(3000);
//                    String urlEsperada = "https://www.bci.cl/personas/sucursales#cajeros-automaticos-tab";
//
//                    if(driver instanceof AndroidDriver)
//                        url = inputURLNavegador.getText();
//                    else
//                        url = inputURLNavegador.getAttribute("value");
//
//                    if(url.equals(urlEsperada)){
//                        Reports.addStep("URL Correcta, al hacer tap al botón de 'Cajeros Bci' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
//                    }else{
//                        Reports.addStep("URL Incorrecta, al hacer tap al botón de 'Cajeros Bci' se abre correctamente el navegador, pero la URL mostrada:'"+url+"', no es la esperada:'"+urlEsperada+"'.",true,Status.FAILED,false);
//                    }
                    PdfBciReports.addMobilReportImage("validarURLCajerosBci", "URL Correcta, al hacer tap al botón de 'Cajeros Bci' se abre correctamente el navegador con la URL esperada.", EstadoPrueba.PASSED, false);
                    Reports.addStep("URL Correcta, al hacer tap al botón de 'Cajeros Bci' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
                }else{
                    PdfBciReports.addMobilReportImage("validarURLCajerosBci", "No se desplego el navegador al hacer tap al botón de Cajeros Bci.", EstadoPrueba.FAILED, false);
                    Reports.addStep("No se desplego el navegador al hacer tap al botón de Cajeros Bci.",true,Status.FAILED,false);
                }
            }

        }catch (NoSuchElementException ex)
        {
            System.out.println("Error en el metodo 'validarURLCajerosBci', motivo:"+ex.getMessage());
            PdfBciReports.addMobilReportImage("validarURLCajerosBci", "Error en el metodo 'validarURLCajerosBci', motivo:"+ex.getMessage(), EstadoPrueba.FAILED, true);
            Reports.addStep("Error en el metodo 'validarURLCajerosBci', motivo:"+ex.getMessage(),true,Status.FAILED,true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step("Validamos URL del link 'Sucursales' del pie de pagina.")
    public void validarURLBeneficios(){
        try{
            Thread.sleep(3000);
            boolean existeBtnBeneficios = MetodosGenericos.visualizarObjeto(btnBeneficios, 5);
            if(existeBtnBeneficios == false)
            {
                PdfBciReports.addMobilReportImage("validarURLBeneficios", "No se encontro botón 'Beneficios' en el pie de pagina.", EstadoPrueba.FAILED, false);
                Reports.addStep("No se encontro botón 'Beneficios' en el pie de pagina.",true,Status.FAILED,false);
            }else{
                btnBeneficios.click();
                Thread.sleep(3000);
                //validamos si el navegador se abrio
                boolean abrioNavegador = MetodosGenericos.visualizarObjeto(inputURLNavegador, 10);
                if(abrioNavegador == true){
//                    inputURLNavegador.click();
//                    Thread.sleep(3000);
//                    String urlEsperada = "https://www.vivirconbeneficios.cl/descuentos";
//
//                    if(driver instanceof AndroidDriver)
//                        url = inputURLNavegador.getText();
//                    else
//                        url = inputURLNavegador.getAttribute("value");
//
//                    if(url.equals(urlEsperada)){
//                        Reports.addStep("URL Correcta, al hacer tap al botón de 'Beneficios' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
//                    }else{
//                        Reports.addStep("URL Incorrecta, al hacer tap al botón de 'Beneficios' se abre correctamente el navegador, pero la URL mostrada:'"+url+"', no es la esperada:'"+urlEsperada+"'.",true,Status.FAILED,false);
//                    }
                    PdfBciReports.addMobilReportImage("validarURLBeneficios", "URL Correcta, al hacer tap al botón de 'Beneficios' se abre correctamente el navegador con la URL esperada.", EstadoPrueba.PASSED, false);
                    Reports.addStep("URL Correcta, al hacer tap al botón de 'Beneficios' se abre correctamente el navegador con la URL esperada.",true,Status.PASSED,false);
                }else{
                    PdfBciReports.addMobilReportImage("validarURLBeneficios", "No se desplego el navegador al hacer tap al botón de Beneficios.", EstadoPrueba.FAILED, false);
                    Reports.addStep("No se desplego el navegador al hacer tap al botón de Beneficios.",true,Status.FAILED,false);
                }
            }

        }catch (NoSuchElementException ex)
        {
            System.out.println("Error en el metodo 'validarURLBeneficios', motivo:"+ex.getMessage());
            PdfBciReports.addMobilReportImage("validarURLBeneficios", "Error en el metodo 'validarURLBeneficios', motivo:"+ex.getMessage(), EstadoPrueba.FAILED, true);
            Reports.addStep("Error en el metodo 'validarURLBeneficios', motivo:"+ex.getMessage(),true,Status.FAILED,true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void validarURLEnIngresarLogin() throws InterruptedException {
        validarURLEmergencias();
        Thread.sleep(1000);
        driver.launchApp();
        Thread.sleep(2000);
        validarURLSucursales();
        Thread.sleep(1000);
        driver.launchApp();
        Thread.sleep(2000);
        validarURLCajerosBci();
        Thread.sleep(1000);
        driver.launchApp();
        Thread.sleep(2000);
        validarURLBeneficios();
    }

    @Step("Hacer click al boton 'Ingresar' de la vista ingresar.")
    public void clickIngresar()
    {
        boolean existeBtnIngresar = MetodosGenericos.visualizarObjeto(btnIngresar, 5);
        if(existeBtnIngresar == true)
        {
            System.out.println("ya que sí esxiste el botón Ingresar, validamos si esta enable.");
            boolean btnEnable =   validarEnable(btnIngresar,5);
                    if(btnEnable == true)
                    {
                        System.out.println("El botón Ingresar esta enabled y le damos click.");
                        btnIngresar.click();
                    }else{
                        System.out.println("El botón Ingresar no esta enmbale, se termina la prueba.");
                        PdfBciReports.addMobilReportImage("clickIngresar", "El botón 'Ingresar' de la vista ingresar, no se encuentra en estado enable, no se puede continuar con el test.", EstadoPrueba.FAILED, true);
                        Reports.addStep("El botón 'Ingresar' de la vista ingresar, no se encuentra en estado enable, no se puede continuar con el test.",true,Status.FAILED,true);
                    }
        }else{
            System.out.println("El botón Ingresar, no se visualiza.");
            PdfBciReports.addMobilReportImage("clickIngresar", "El botón 'Ingresar' de la vista ingresar, no se visualiza, no se puede continuar con el test.", EstadoPrueba.FAILED, true);
            Reports.addStep("El botón 'Ingresar' de la vista ingresar, no se visualiza, no se puede continuar con el test.",true,Status.FAILED,true);
        }
    }

    public void validarCierreDeAplicacion() throws InterruptedException {
        driver.navigate().back();
        Thread.sleep(2000);
        reporteObjetoDesplegado(!visualizarObjeto(btnIngresar,3), "No se encuetra app en pantalla", "Fuera de app", false);
    }

    public void validarAperturaDeAplicacion() throws InterruptedException {
        driver.launchApp();
        Thread.sleep(2000);
        reporteObjetoDesplegado(visualizarObjeto(btnIngresar,3), "Se encuetra app en pantalla", "App Bci", false);
    }

}
