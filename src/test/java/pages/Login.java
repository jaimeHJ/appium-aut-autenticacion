package pages;

import constants.Constants;
import driver.DriverContext;
import genericPages.TecladoRutPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import utils.MetodosGenericos;

import static util.MetodosGenericos.compararTextoConReporte;
import static util.MetodosGenericos.validarEnable;
import static util.MetodosGenericos.validaAlias;
import static utils.MetodosGenericos.*;
import genericPages.LoginPage.*;

public class Login {

    AppiumDriver driver;
    public Login() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }


    //===========OBJETOS==========================================================================================================================

    //HOLA ALIAS
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tv_welcome_user")
    @iOSFindBy(xpath = "//*[contains(@name,\"Hola \")]")
    public MobileElement textoAlias;

    //NO ERES TU
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/tv_change_user")
    @iOSFindBy(id = "AppLoginViewController.btnNoEresTu")
    public MobileElement cambiarUsuario;

    //BOTON login
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/btn_login")
    @iOSFindBy(id = "AppLoginViewController.btnLogin")
    public MobileElement btnLogin;

    //Ingresa Tu Rut
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/et_user_rut")
    @iOSFindBy(id = "AppLoginViewController.txtRut")
    public MobileElement inputRut;

    //Password
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/et_user_password")
    @iOSFindBy(id = "AppLoginViewController.txtPass")
    public MobileElement inputPassword;

    //Label Error de ingreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/textinput_error")
    //@iOSFindBy(xpath = "//*[contains(@name,\"Por favor \")]")
    public MobileElement lblErrorDeIngreso;

    //BOTON BACK, DE LOGIN CUANDO YA SE LOGUEO CON ALIAS
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/ib_back")
    @iOSFindBy(id = "AppLoginViewController.btnBack")
    public MobileElement btnBackLogin;

    //OJO campo contraseña
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/text_input_password_toggle")
    @iOSFindBy(id = "AppLoginViewController.btnEye")
    public MobileElement btnVerPassword;

    //LINK PROBLEMAS CONTU CLAVE
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID +":id/forgotten_password_link")
    @iOSFindBy(id = "AppLoginViewController.problemasDeClave")
    public MobileElement linkProblemasClave;


    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"Permitir\"]")
    public MobileElement btnPermisoInicial;


    //===========METODOS==========================================================================================================================

    @Step("Loguearse con Alias, sólo ingresando contraseña.")
    public void loginConAlias(String pass){
        inputPassword.setValue(pass);
    }

    @Step("Validar apertura de la vista Ingresar")
    public  void validarInicio(){
        reporteObjetoDesplegado(visualizarObjeto(inputPassword,10), "Pantalla LoginNormal", "LoginNormal", false);
    }

    @Step("Validar objetos de la vista LoginNormal con Alias, con un usuario ya logueado previamente.")
    public void validarObjetosVistaLogin(boolean conAlias)
    {
        System.out.println("Validar objetos vista login");
        reporteObjetoDesplegado(visualizarObjeto(btnBackLogin,5), "Link de Problemas con tu clave","LoginNormal", false);
        if(conAlias) {
            reporteObjetoDesplegado(validaAlias(textoAlias.getText()), "Alias con texto: Hola *", "Ingresar a LoginNormal", false);
            compararTextoConReporte(cambiarUsuario, "¿No eres tú?");
        }
        else
            reporteObjetoDesplegado(visualizarObjeto(inputRut, 5), "Password", "LoginNormal", false);//compararTextoConReporte(inputRut,"Ingresa tu RUT");

        reporteObjetoDesplegado(visualizarObjeto(inputPassword, 5), "Password", "LoginNormal", false);//compararTextoConReporte(inputPassword,"Clave Internet");
        reporteObjetoDesplegado(visualizarObjeto(btnVerPassword,5), "Ojo para ver caracteres del campo contraseña","LoginNormal", false);
        compararTextoConReporte(linkProblemasClave, "¿Problemas con tu clave?");
        compararTextoConReporte(btnLogin, "INGRESAR");
        reporteObjetoDesplegado(!validarEnable(btnLogin, 5), "Botón Ingresar deshabilitado", "LoginNormal", false);
    }

    @Step("Ingresar Datos en el LoginNormal con alias.")
    public void ingresarDatosLoginConAlias(String password){
        ingresarDatos("conAlias", password);
    }

    @Step("Ingresar Datos en el LoginNormal.")
    public void ingresarDatosLogin(String rut, String password){
        ingresarDatos(rut, password);
    }

    @Step("Ingresar Datos en el LoginNormal e iniciamos session.")
    public void ingresarDatos(String rut, String pass){
        if(!rut.equals("conAlias"))
        {
            if (DriverContext.getDriver() instanceof AndroidDriver) {
                this.inputRut.setValue(rut);
            } else {
                TecladoRutPage tecladoRutPage = new TecladoRutPage();
                tecladoRutPage.getKeyboardPoints();
                tecladoRutPage.sendRut(rut);
            }
        }

        inputPassword.clear();
        inputPassword.setValue(pass);
        System.out.println("Clave ingresada");
        reporteObjetoDesplegado(validarEnable(btnLogin, 5), "Botón Ingresar Habilitado", "LoginNormal", false);
        btnLogin.click();
    }

    @Step("Ingresar Datos erróneos.")
    public void ingresarDatosErroneos()
    {
        inputRut.clear();
        ingresarRut("12345996");
        inputPassword.click();
        inputPassword.setValue("123");
        System.out.println("Datos ingresados de forma errónea");
        if(DriverContext.getDriver() instanceof AndroidDriver)
            compararTextoConReporte(lblErrorDeIngreso, "Por favor revisa tus datos e ingrésalos nuevamente");

        reporteObjetoDesplegado(!validarEnable(btnLogin, 5), "Botón Ingresar Deshabilitado al ingresar de forma errónea", "LoginNormal", false);
    }

    @Step("Ingresar Datos en blanco.")
    public void ingresarDatosEnBlanco(){
        inputRut.clear();
        inputPassword.clear();
        inputPassword.click();
        System.out.println("Datos ingresados en blanco");
        reporteObjetoDesplegado(!validarEnable(btnLogin, 5), "Botón Ingresar Deshabilitado al ingresar en blanco", "LoginNormal", false);
    }

    @Step("Ingresar y validar datos válidos")
    public void ingresarYValidarDatosValidos(){
        String clave = "111222";
        inputRut.click();
        ingresarRut("100423235");
        inputPassword.setValue(clave);
        System.out.println("Datos válidos ingresados");
        reporteObjetoDesplegado(validarEnable(btnLogin, 5), "Botón Ingresar Habilitado al ingresar datos válidos", "LoginNormal", false);
        btnVerPassword.click();
        compararTextoConReporte(inputPassword, clave);
        btnLogin.click();
    }

    @Step("Ingresar clave incorrecta")
    public void ingresarDatosConClaveIncorrecta(){
        inputRut.clear();
        ingresarRut("10042323-5");
        inputPassword.click();
        inputPassword.setValue("947381");
        System.out.println("Clave incorrecta ingresada");
    }

    @Step("Presionar no eres tú")
    public void presionarNoEresTu(){
        cambiarUsuario.click();
    }

    @Step("Volver atrás")
    public void presionarBack(){
        btnBackLogin.click();
    }

    @Step("Presionar ingresar")
    public void presionarIngresar(){
        btnLogin.click();
    }

    private void ingresarRut(String usr) {
        if (DriverContext.getDriver() instanceof AndroidDriver) {
            this.inputRut.setValue(usr);
        } else {
            TecladoRutPage tecladoRutPage = new TecladoRutPage();
            tecladoRutPage.getKeyboardPoints();
            tecladoRutPage.sendRut(usr);
        }

    }

    public void dismissAllerts()
    {
        if (this.driver instanceof IOSDriver) {
            try {
                btnPermisoInicial.click();
            } catch (NoAlertPresentException var2)
            {
                PdfBciReports.addMobilReportImage("dismissAllerts","No se desplego ningun alert", EstadoPrueba.PASSED, false);
                Reports.addStep("No se desplego ningun alert", true, Status.PASSED, false);
            }
        } else {
            System.out.println("No es un dispositivo IOS, se ignorará esta accion");
        }

    }






}
