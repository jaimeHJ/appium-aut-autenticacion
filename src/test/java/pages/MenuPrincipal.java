package pages;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import utils.MetodosGenericos;

import static utils.MetodosGenericos.reporteObjetoDesplegado;
import static utils.MetodosGenericos.visualizarObjeto;

public class MenuPrincipal
{
    AppiumDriver driver;

    public MenuPrincipal()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //===========OBJETOS==========================================================================================================================

    @AndroidFindBy(className = "android.widget.ImageButton")
    @iOSFindBy(id = "DashboardNavBarView.menuButton")
    public MobileElement btnMenuLateral;

    //@AndroidFindBy(className = "")
    @iOSFindBy(id = "ButtonTabBarIndex1")
    public MobileElement btnTarjetas;


    //===========METODOS==========================================================================================================================

    @Step("Validar apertura de menu.")
    public void validarInicio(){
        reporteObjetoDesplegado(visualizarObjeto(btnMenuLateral, 10), "Inicio luego de LoginNormal", "Menu principal", false);
    }

    @Step("Abrir menu lateral.")
    public void clickMenuLateral()
    {
        boolean existeMenu = visualizarObjeto(btnMenuLateral, 10);
        if(existeMenu == true)
        {
            System.out.println("Abrimos menu lateral");
            btnMenuLateral.click();
        }else{
            PdfBciReports.addMobilReportImage("clickMenuLateral","No se visualiza botón para abrir menu lateral.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza botón para abrir menu lateral.",true, Status.FAILED,true);
        }
    }

    @Step
    public void clickBtnTarjetas()
    {
        boolean existeBtnTarjetas = visualizarObjeto(btnTarjetas, 10);

        if(existeBtnTarjetas == true)
        {
            System.out.println("Abrimos Tarjetas.");
            btnTarjetas.click();

        }else{

            PdfBciReports.addMobilReportImage("clickBtnTarjetas","No se visualiza botón para abrir menu lateral.", EstadoPrueba.FAILED,true);
            Reports.addStep("No se visualiza botón para abrir menu lateral.",true, Status.FAILED,true);
        }
    }


}
