package pages.RecargaCelular;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class ListadodeNumerosInscritos
{
    AppiumDriver driver;
    public ListadodeNumerosInscritos()
    {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //@AndroidFindBy(xpath = "//*[contains(@text,\"Recarga celular\")]")
    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/toolbar_title")
    //@iOSFindBy
    public MobileElement lblNumerosInscritos;

    //Flecha hacia atras sin resourceid ni text
    //@AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/toolbar_title")
    //@iOSFindBy
    //public MobileElement lblNumerosInscritos;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_alias_group_title")
    //@iOSFindBy
    public MobileElement lblTituloGrupo;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_alias_name")
    //@iOSFindBy
    public MobileElement lblAliasInscrito;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_alias_number")
    //@iOSFindBy
    public MobileElement lblNumeroInscrito;


}
