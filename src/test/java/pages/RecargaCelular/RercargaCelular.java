package pages.RecargaCelular;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;

public class RercargaCelular {

    AppiumDriver driver;
    public RercargaCelular() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }
    @AndroidFindBy(xpath = "//*[contains(@text,\"Recarga celular\")]")
    //@AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/tv_dashboard_pago_cuentas")
    //@iOSFindBy
    public MobileElement lblRecargaCelular;


    @AndroidFindBy(xpath = "//*[contains(@text,\"Recarga rápido a tus números inscritos\")]")
    //@iOSFindBy
    public MobileElement lblDescripcionRecarga;



    @AndroidFindBy(xpath = "//*[contains(@text,\"Ver todos los números inscritos\")]")
    //@iOSFindBy
    public MobileElement BtnVerNumerosInscritos;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/first_frequent_nick")
    //@iOSFindBy
    public MobileElement lblRecargaInscritaUno;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/second_frequent_nick")
    //@iOSFindBy
    public MobileElement lblRecargaInscritaDos;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/third_frequent_nick")
    //@iOSFindBy
    public MobileElement lblRecargaInscritaTres;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/new_recharge_nick")
    //@iOSFindBy
    public MobileElement lblNuevaRecarga;


    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/new_recharge_nick2")
    //@iOSFindBy
    public MobileElement lblNuevaRecarga2;


}
