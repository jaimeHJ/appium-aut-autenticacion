package pages.RecargaCelular;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class RecargaInscrito {

    AppiumDriver driver;
    public RecargaInscrito() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver),this);
    }

    //@AndroidFindBy(xpath = "//*[contains(@text,\"Recargar número inscrito\")]")
    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/toolbar_title")
    //@iOSFindBy
    public MobileElement lblRecargarnumeroInscrito;

    //Flecha hacia atras sin resourceid ni text
    //@AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/toolbar_title")
    //@iOSFindBy
    //public MobileElement lblNumerosInscritos;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_recharge_name")
    //@iOSFindBy
    public MobileElement lblNombreRecargaFrecuente;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_recharge_number")
    //@iOSFindBy
    public MobileElement lblNumeroRecargaFrecuente;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_recharge_company")
    //@iOSFindBy
    public MobileElement lblCompaniaRecargaFrecuente;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_recharge_bank_account_selector")
    //@iOSFindBy
    public MobileElement BoxSelectorDeCuentas;

    @AndroidFindBy(id= Constants.BCI_APP_PACKAGE_ID+":id/frequent_recharge_bank_account_selector_action")
    //@iOSFindBy
    public MobileElement BtnCambiarCuenta;

    








}
