package bd;

import Exceptions.SmokeTestException;
import constants.ConstantesAutenticacion;
import dataSources.DbUser;
import dataSources.SyBase;
import driver.DriverContext;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.qameta.allure.model.Status;
import reporter.Reports;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import static org.bouncycastle.util.Strings.toLowerCase;
import static util.MetodosGenericos.rutParaQuery;

public class SybaseJournal extends SyBase {
    private static String query;

    public static void limpiarJournal(String rut) {
        String fechaActual = LocalDate.now().toString();
        String sistema = "";
        if (DriverContext.getDriver() instanceof IOSDriver)
            sistema = "ios";
        else if (DriverContext.getDriver() instanceof AndroidDriver)
            sistema = "android";

        query = "Delete from journal.dbo.jen " +
                "where jen_rut_cli = '" + rutParaQuery(rut) + "' " +
                "and jen_fec_evt_neg > '" + fechaActual + "' " +
                "and jen_med = '" + toLowerCase(sistema) + "'";
        ejecutarQuery(query);

    }

    public static void validarJournal(String rut) throws InterruptedException {
        String fechaActual = LocalDate.now().toString();
        String sistema = "";
        if (DriverContext.getDriver() instanceof IOSDriver)
            sistema = "ios";
        else if (DriverContext.getDriver() instanceof AndroidDriver)
            sistema = "android";

        query = "Select * from journal.dbo.jen " +
                "where jen_rut_cli = '" + rutParaQuery(rut) + "' " +
                "and jen_fec_evt_neg > '" + fechaActual + "' " +
                "and jen_id_cnl = 'MOVIBCINAT' " +
                "and jen_id_evt = 'LOGIN' " +
                "and jen_med = '" + toLowerCase(sistema) + "' order by jen_fec_evt_neg DESC";
        ejecutarQueryConResultado(query);
    }

    public static void ejecutarQueryConResultado(String query) throws InterruptedException
    {
        Thread.sleep(10000);
        try {
            Connection con = getConnection(DbUser.getUser("sybaseJournal.properties"));
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            System.out.println("Se realizó la siguiente consulta en Sybase:\n" + query);
            Reports.addStep("Se realizó la siguiente consulta en Sybase: " + query, false, Status.PASSED, false);
            if (rs.next()) {
                System.out.println("Ejecución contenía Registros");
                Reports.addStep("[SybaseJournal] Existe el registro en la Base de Datos journal, tabla 'jen' según consulta.", false, Status.PASSED, false);
            } else {
                System.out.println("Ejecución no contenía Registros");
                Reports.addStep("[SybaseJournal] No existe el registro en la Base de Datos journal, tabla 'jen' según consulta.", false, Status.FAILED, false);
            }

        } catch (SQLException sqe) {
            System.out.println("Unexpected exception : " +
                    sqe.toString() + ", sqlstate = " +
                    sqe.getSQLState());
            Reports.addStep("[SybaseJournal] Error: al consultar el registro en la Base de Datos 'journal', tabla ‘jen’ según consulta [" +
                    query + "] " + sqe.getMessage(), false, Status.FAILED, false);
        }
    }

    public static void ejecutarQuery(String query){
        try {
            Connection con = getConnection(DbUser.getUser("sybaseJournal.properties"));
            Statement stmt = con.createStatement();
            System.out.println("Se ejecuta la siguiente query en Sybase:\n" + query);
            if (stmt.execute(query)) {
                System.out.println("Ejecución contenía registro");
                Reports.addStep("Se ejecuta la siguiente query en Sybase con registro: " + query, false, Status.PASSED, false);
            } else {
                System.out.println("Ejecución sin registro");
                Reports.addStep("Se ejecuta la siguiente query en Sybase sin registro: " + query, false, Status.PASSED, false);
            }
        } catch (SQLException sqe) {
            System.out.println("Unexpected exception : " +
                    sqe.toString() + ", sqlstate = " +
                    sqe.getSQLState());
            Reports.addStep("[SybaseJournal] Error: al ejecutar query en la Base de Datos 'journal', tabla ‘jen’ según consulta [" +
                    query + "] " + sqe.getMessage(), false, Status.FAILED, false);
        }
    }

}
