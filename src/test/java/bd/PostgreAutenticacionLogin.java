package bd;

import constants.ConstantesAutenticacion;
import dataSources.DbUser;
import dataSources.Postgre;
import io.qameta.allure.model.Status;
import reporter.Reports;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import static java.lang.Integer.parseInt;

public class PostgreAutenticacionLogin extends Postgre {
    public static int getIdCliente(String rut){
        String fechaActual = LocalDate.now().toString();
        String id = "0";
        String query = "SELECT id from clientes where rut = '" + rut + "'";
        try {
            Connection con = getConnection(DbUser.getUser("postgreAutenticacionLogin.properties"));
            if (con == null) {
                System.out.println("Conexion nula");
            }

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while(rs.next()) {
                id = rs.getString("id");
                System.out.println("BD ID: " + id);
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }
        return parseInt(id);
    }

    public static void deleteLogsDispositivos(String rut){

        String query = "DELETE FROM public.logs_dispositivos WHERE dispositivo_id in (SELECT id FROM dispositivos where cliente_id in (SELECT id FROM clientes where rut = '"+rut+"'))";
        try {
            Connection con = getConnection(DbUser.getUser("postgreAutenticacionLogin.properties"));
            if (con == null) {
                System.out.println("Conexion nula");
            }

            Statement st = con.createStatement();
            st.executeUpdate(query);
            con.close();
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }

    public static void deleteDispositivos(String rut){

        String query = "DELETE FROM dispositivos where cliente_id in (SELECT id FROM clientes where rut = '"+rut+"')";
        try {
            Connection con = getConnection(DbUser.getUser("postgreAutenticacionLogin.properties"));
            if (con == null) {
                System.out.println("Conexion nula");
            }

            Statement st = con.createStatement();
            st.executeUpdate(query);
            con.close();
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }

    public static boolean dispositivos(String rut) {
        String id = "0";
        String query = "Select id from dispositivos where cliente_id in (SELECT id FROM clientes where rut = '"+rut+"')";
        try {
            Connection con = getConnection(DbUser.getUser("postgreAutenticacionLogin.properties"));
            if (con == null) {
                System.out.println("Conexion nula");
            }

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while(rs.next()) {
                id = rs.getString("id");
                System.out.println("BD ID: " + id);
            }
            con.close();
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        if (id != null) {
            Reports.addStep("se encontro un elemento en la base de datos", Status.PASSED, false);
        }else{
            Reports.addStep("No se encontro un elemento en la base de datos", Status.FAILED, true);
        }
        return true;

    }


}

