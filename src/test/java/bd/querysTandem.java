package bd;

import Exceptions.SmokeTestException;
import dataSources.DbUser;
import dataSources.Tandem;
import constants.ConstantesAutenticacion;
import io.qameta.allure.model.Status;
import reporter.Reports;
import utils.MetodosGenericos;

import java.io.IOException;
import java.io.PrintStream;

import static constants.ConstantesAutenticacion.TANDEM_PSW;
import static constants.ConstantesAutenticacion.TANDEM_USER;
import static dataSources.TelnetManager.readUntil;


public class querysTandem extends Tandem{

    private static void loginTandem()
    {
            Tandem.login(DbUser.getUser("dbTandem.properties"));
            //login(ConstantesAutenticacion.TANDEM_USER, ConstantesAutenticacion.TANDEM_PSW,ConstantesAutenticacion.SMOKE_TEST);
    }


    public static void bloquearClave(String rut) throws IOException, InterruptedException {

        //formateamos el rut
        rut =  MetodosGenericos.formatearRutTandem(rut);
        //Iniciamos Tandem
        loginTandem();
//        login("QATDM.EXJHUEN","Agosto2018");
        //Query: Modificar, Bloquear la clave de un usuario
        String query="Update =tbcse01 set CSE_COD_EST='01', CSE_MOT_EST='', \n"+
                "CSE_ING_ERR_ACU=0,CSE_ING_ERR_DIA=0 \n"+
                "where CSE_RUT_NRO_USR='"+rut+"' and CSE_IDN_GRP_CNL='03';";
        write(query);

        String resultado = readUntil("updated.",10000);
        if(resultado == null){
            Reports.addStep("No se realiza el bloqueo de la clave para el rut: "+rut,false, Status.FAILED,false);
            System.out.println("No se realiza el bloqueo de la clave para el rut: "+rut);
        }else{
            Reports.addStep("Se realiza el bloqueo de la clave para el rut: "+rut,false, Status.PASSED,false);
            System.out.println("Se realiza el bloqueo de la clave para el rut: "+rut);
        }

        disconnect();
        //System.out.println("se bloquea clave del usuario con rut:"+rut);

    }


    public static void desbloquearClave(String rut) throws IOException, InterruptedException {

        //formateamos el rut
        rut =  MetodosGenericos.formatearRutTandem(rut);
        //Iniciamos Tandem
//        login(TANDEM_USER, TANDEM_PSW);
        loginTandem();
        //Query: Modificar, Desbloquear la clave de un usuario

        String query="Update =tbcse01 set CSE_COD_EST='00', CSE_MOT_EST='', \n" +
                "CSE_ING_ERR_ACU=0,CSE_ING_ERR_DIA=0 \n" +
                "where CSE_RUT_NRO_USR='"+rut+"' and CSE_IDN_GRP_CNL='03';";
        write(query);
        //String query="Update =tbcse01 set CSE_COD_EST='00', CSE_MOT_EST='', CSE_ING_ERR_ACU=0,CSE_ING_ERR_DIA=0 where CSE_RUT_NRO_USR='"+rut+" ' and CSE_IDN_GRP_CNL='03';";


        String resultado = readUntil("updated.",10000);
        if(resultado != null){
            Reports.addStep("Se realiza el desbloqueo de la clave para el rut: "+rut,false, Status.PASSED,false);
            System.out.println("Se realiza el desbloqueo de la clave para el rut: "+rut);
        }else{
            Reports.addStep("No se realiza el desbloqueo de la clave para el rut: "+rut, false, Status.FAILED,false);
            System.out.println("No se realiza el desbloqueo de la clave para el rut: "+rut);
        }
        disconnect();
    }

    public static void vencerClave(String rut) throws IOException, InterruptedException {

        //formateamos el rut
        rut =  MetodosGenericos.formatearRutTandem(rut);
        //Iniciamos Tandem
//        login(TANDEM_USER, TANDEM_PSW);
        loginTandem();
        //Query: Modificar, Desbloquear la clave de un usuario



        String query="Update =tbcse01 set CSE_FEC_EXP= date '2015-12-31'\n" +
                     "where CSE_RUT_NRO_USR='"+rut+"';";
        write(query);
        //String query="Update =tbcse01 set CSE_COD_EST='00', CSE_MOT_EST='', CSE_ING_ERR_ACU=0,CSE_ING_ERR_DIA=0 where CSE_RUT_NRO_USR='"+rut+" ' and CSE_IDN_GRP_CNL='03';";


        String resultado = readUntil("updated.",10000);
        if(resultado != null){
            Reports.addStep("Se realiza el vencimiento de la clave para el rut: "+rut,false, Status.PASSED,false);
            System.out.println("Se realiza el vencimiento de la clave para el rut: "+rut);
        }else{
            Reports.addStep("No se realiza el vencimiento de la clave para el rut: "+rut, false, Status.FAILED,false);
            System.out.println("No se realiza el vencimiento de la clave para el rut: "+rut);
        }
        disconnect();
    }

    public static void reverzarVencimiento(String rut) throws IOException, InterruptedException {

        //formateamos el rut
        rut =  MetodosGenericos.formatearRutTandem(rut);
        //Iniciamos Tandem
//        login(TANDEM_USER, TANDEM_PSW);
        loginTandem();
        //Query: Modificar, Desbloquear la clave de un usuario



        String query="Update =tbcse01 set CSE_FEC_EXP = date'2019-12-31'\n" +
                "where CSE_RUT_NRO_USR='"+rut+"';";
        write(query);
        //String query="Update =tbcse01 set CSE_COD_EST='00', CSE_MOT_EST='', CSE_ING_ERR_ACU=0,CSE_ING_ERR_DIA=0 where CSE_RUT_NRO_USR='"+rut+" ' and CSE_IDN_GRP_CNL='03';";


        String resultado = readUntil("updated.",10000);
        if(resultado != null){
            Reports.addStep("Se realiza el vencimiento de la clave para el rut: "+rut,false, Status.PASSED,false);
            System.out.println("Se reverza el vencimiento de la clave para el rut: "+rut);
        }else{
            Reports.addStep("No se realiza el vencimiento de la clave para el rut: "+rut, false, Status.FAILED,false);
            System.out.println("No se realiza el vencimiento de la clave para el rut: "+rut);
        }
        disconnect();
    }
}
