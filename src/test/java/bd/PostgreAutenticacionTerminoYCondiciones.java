package bd;

import constants.ConstantesAutenticacion;
import dataSources.DbUser;
import dataSources.Postgre;
import io.qameta.allure.model.Status;
import org.testng.Assert;
import org.testng.annotations.Test;
import reporter.Reports;

import java.sql.*;

import io.qameta.allure.model.Status;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;

import org.testng.Assert;
import reporter.Reports;
public class PostgreAutenticacionTerminoYCondiciones extends Postgre {
    public static void validarTerminoYCondiciones(int idCliente){
        String fechaActual = LocalDate.now().toString();
        String query = "SELECT id, cliente_id, terminosycondiciones_id, fecha " +
                "from public.historial_cliente " +
                "where cliente_id = " + idCliente;

        try {
            Connection con = getConnection(DbUser.getUser("postgreAutenticacionTerminoYCondiciones.properties"));
            if (con == null) {
                System.out.println("Conexion nula");
            }

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            if(rs.next())
                Reports.addStep("Se ejecuta la siguiente query en Postgres: " + query, false, Status.PASSED, false);
            else
                Reports.addStep("No es posible ejecutar la siguiente query en Postgres: " + query, false, Status.FAILED, false);
        } catch (Exception var4) {
            var4.printStackTrace();
        }
    }
}
