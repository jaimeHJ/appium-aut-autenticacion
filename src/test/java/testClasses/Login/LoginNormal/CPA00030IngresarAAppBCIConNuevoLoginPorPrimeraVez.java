package testClasses.Login.LoginNormal;

import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;
import reporter.Reports;

import static bd.SybaseJournal.limpiarJournal;
import static bd.SybaseJournal.validarJournal;
import static testbase.TestBase.cerrarSesion;
import static testbase.TestBase.loginRapido;

public class CPA00030IngresarAAppBCIConNuevoLoginPorPrimeraVez {
    public void pruebaAutomatizada() throws Exception {

        String rut = "10042323-5";

        //Limpiar journal antes de Ingresar
        limpiarJournal(rut);

        //Ingresar a LoginNormal
        IngresarALogin pageIngresarALogin = new IngresarALogin();
        pageIngresarALogin.validarInicio();

        //Ingresar a LoginNormal
        pageIngresarALogin.clickIngresar();

        //LoginNormal
        Login pageLogin = new Login();
        pageLogin.validarInicio();
        pageLogin.validarObjetosVistaLogin(false);
        pageLogin.presionarBack();

        //Ingresar a LoginNormal
        pageIngresarALogin.validarInicio();
        pageIngresarALogin.clickIngresar();

        //LoginNormal
        pageLogin.validarInicio();
        pageLogin.ingresarDatosErroneos();
        pageLogin.ingresarDatosEnBlanco();
        pageLogin.ingresarYValidarDatosValidos();
        pageLogin.dismissAllerts();

        //Terminos y condiciones.
        TerminosYCondiciones pageTerminosYCondiciones = new TerminosYCondiciones();
        pageTerminosYCondiciones.validarInicio();

        //Validar Journal Sybase
        validarJournal(rut);

        Reports.finalAssert();
    }
}
