package testClasses.Login.LoginNormal;

import driver.DriverContext;
import io.appium.java_client.android.AndroidDriver;
import pages.IngresarALogin;
import pages.Login;
import pages.ModalClaveBloqueada;
import pages.ModalDatosIngresadosIncorrectos;
import reporter.Reports;

import static bd.querysTandem.desbloquearClave;

public class CPA00034RechazarIngresoTercerIntentoEnNuevoLoginDeAppBCI {
    public void pruebaAutomatizada() throws Exception {

        //Desbloquear cliente
        desbloquearClave("10042323-5");

        //Ingresar a LoginNormal
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();

        //Ingresar a LoginNormal
        ingresarALogin.clickIngresar();

        //LoginNormal
        Login pageLogin = new Login();
        pageLogin.validarInicio();
        pageLogin.ingresarDatosConClaveIncorrecta();
        pageLogin.presionarIngresar();

        //Modal Datos ingresados incorrectamente
        ModalDatosIngresadosIncorrectos pageModalDatosIngresadosIncorrectos = new ModalDatosIngresadosIncorrectos();
        pageModalDatosIngresadosIncorrectos.validarObjetosModal();
        pageModalDatosIngresadosIncorrectos.presionarAceptar();

        //LoginNormal
        //pageLogin.ingresarDatosConClaveIncorrecta();
        pageLogin.presionarIngresar();

        //Modal Datos ingresados incorrectamente
        pageModalDatosIngresadosIncorrectos.validarObjetosModal();
        pageModalDatosIngresadosIncorrectos.presionarAceptar();

        //LoginNormal
        //pageLogin.ingresarDatosConClaveIncorrecta();
        pageLogin.presionarIngresar();

        //Modal Clave bloqueada
        ModalClaveBloqueada pageModalClaveBloqueada = new ModalClaveBloqueada();
        pageModalClaveBloqueada.validarObjetosModal();

        //Desbloquear cliente
        desbloquearClave("10042323-5");

        Reports.finalAssert();
    }
}
