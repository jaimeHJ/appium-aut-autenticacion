package testClasses.Login.LoginNormal;

import pages.IngresarALogin;
import pages.Login;
import pages.ModalClaveBloqueada;
import pages.ModalDatosIngresadosIncorrectos;
import reporter.Reports;

import static bd.querysTandem.desbloquearClave;

public class CPA00033RechazarIngresoSegundoIntentoEnNuevoLoginDeAppBCI {
    public void pruebaAutomatizada() throws Exception {

        //Desbloquear cliente
        desbloquearClave("10042323-5");

        //Ingresar a LoginNormal
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();

        //Ingresar a LoginNormal
        ingresarALogin.clickIngresar();

        //LoginNormal
        Login pageLogin = new Login();
        pageLogin.validarInicio();
        pageLogin.ingresarDatosConClaveIncorrecta();
        pageLogin.presionarIngresar();

        //Modal Datos ingresados incorrectamente
        ModalDatosIngresadosIncorrectos pageModalDatosIngresadosIncorrectos = new ModalDatosIngresadosIncorrectos();
        pageModalDatosIngresadosIncorrectos.validarObjetosModal();
        pageModalDatosIngresadosIncorrectos.presionarAceptar();

        //LoginNormal
        pageLogin.ingresarDatosConClaveIncorrecta();
        pageLogin.presionarIngresar();

        //Modal Datos ingresados incorrectamente
        pageModalDatosIngresadosIncorrectos.validarObjetosModal();

        Reports.finalAssert();
    }
}
