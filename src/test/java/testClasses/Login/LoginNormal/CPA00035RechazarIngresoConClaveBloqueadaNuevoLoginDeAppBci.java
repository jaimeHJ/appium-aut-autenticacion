package testClasses.Login.LoginNormal;

import pages.IngresarALogin;
import pages.Login;
import pages.ModalClaveBloqueada;
import reporter.Reports;

import static bd.querysTandem.*;

public class CPA00035RechazarIngresoConClaveBloqueadaNuevoLoginDeAppBci {
    public void pruebaAutomatizada() throws Exception {

        //Desbloquear cliente
        bloquearClave("10042323-5");

        //Ingresar a LoginNormal
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();

        //Ingresar a LoginNormal
        ingresarALogin.clickIngresar();

        //LoginNormal
        Login pageLogin = new Login();
        pageLogin.validarInicio();
        pageLogin.ingresarDatosConClaveIncorrecta();
        pageLogin.presionarIngresar();


        //Modal Clave bloqueada
        ModalClaveBloqueada pageModalClaveBloqueada = new ModalClaveBloqueada();
        pageModalClaveBloqueada.validarObjetosModal();

        //Desbloquear cliente
        desbloquearClave("10042323-5");

        Reports.finalAssert();
    }
}
