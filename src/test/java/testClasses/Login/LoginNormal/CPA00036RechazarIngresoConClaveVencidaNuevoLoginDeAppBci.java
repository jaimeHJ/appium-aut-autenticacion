package testClasses.Login.LoginNormal;

import pages.IngresarALogin;
import pages.Login;
import pages.ModalClaveVencida;
import reporter.Reports;

import static bd.querysTandem.*;


public class CPA00036RechazarIngresoConClaveVencidaNuevoLoginDeAppBci {
    public void pruebaAutomatizada() throws Exception {

        //Desbloquear cliente
        vencerClave("10042323-5");

        //Ingresar a LoginNormal
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();

        //Ingresar a LoginNormal
        ingresarALogin.clickIngresar();

        //LoginNormal
        Login pageLogin = new Login();
        pageLogin.validarInicio();
        pageLogin.ingresarYValidarDatosValidos();
        //pageLogin.presionarIngresar();


        //Modal Clave bloqueada
        ModalClaveVencida pageModalClaveVencida = new ModalClaveVencida();
        pageModalClaveVencida.validarObjetosModalClaveVencida();
        pageModalClaveVencida.validarURLGenerarClaveVencida();

        //Desbloquear cliente
        reverzarVencimiento("10042323-5");

        Reports.finalAssert();
    }

}
