package testClasses.Login.LoginNormal;

import driver.DriverContext;
import io.appium.java_client.android.AndroidDriver;
import pages.IngresarALogin;
import reporter.Reports;

public class CPA00029DesplegarAppBCIConNuevoLogin {
    public void pruebaAutomatizada() throws Exception {

    //Ingresar a LoginNormal
    IngresarALogin ingresarALogin = new IngresarALogin();
    ingresarALogin.validarInicio();

    if(DriverContext.getDriver() instanceof AndroidDriver) {
        ingresarALogin.validarCierreDeAplicacion();
        ingresarALogin.validarAperturaDeAplicacion();
    }

    ingresarALogin.validarObjetosVistaIngresar(false);
    ingresarALogin.validarURLEnIngresarLogin();
    Reports.finalAssert();
    }
}
