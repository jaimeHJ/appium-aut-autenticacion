package testClasses.Login.TerminosyCondiciones;

import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;

public class CPA00002AceptarTérminosyCondicionesLoginEnAppBCIconNuevoLogin {


    public void pruebaAutomatizada(){
        //Damos click al boton ingresar
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //Ingresamos datos en el login e ingresamos
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin("10042323-5","111222");

        //Validar links y aceptar terminos y condiciones
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.validarInicio();
        TYC.validarObjetosModalTYC();
        TYC.aceptarTerminosYCondiciones();

        //Validación Postgre
        // validarTerminoYCondiciones(getIdCliente("4098198-5")) ;
    }
}
