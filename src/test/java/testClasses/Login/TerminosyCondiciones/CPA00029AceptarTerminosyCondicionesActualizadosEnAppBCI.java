package testClasses.Login.TerminosyCondiciones;

import bd.SybaseJournal;
import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;

import static testbase.TestBase.cerrarSesion;
import static testbase.TestBase.loginRapido;

public class  CPA00029AceptarTerminosyCondicionesActualizadosEnAppBCI {

    public void pruebaAutomatizada() throws InterruptedException{
        //Hacemos login rapido por las precondiciones del caso
        Login login = new Login();
        String rut ="10042323-5";
        String pass = "111222";
        loginRapido(rut,pass);
        cerrarSesion();
        login.presionarBack ();
        login.presionarBack ();
        /********************************************************
         * SE DEBE GENERAR QWERY PARA LOS TÉRMINOS Y CONDICIONES *
         * Y SELECCIONAR UNA VERSIÓN ANTIGUA DE TYC              *
         * SE UTILIZAN LAS QWERYS ENCONTRADAS EN EL CASO DE UFT  *
         ********************************************************/
        SybaseJournal.limpiarJournal(rut);
        SybaseJournal.ejecutarQueryConResultado("select  version FROM public.terminosycondiciones where vigente = true;");
        SybaseJournal.ejecutarQuery("insert into public.terminosycondiciones (version, fecha_vigencia, contenido, vigente) values ('30.0','2018-10-16','https://www.bci.cl/corporacion/politicas-de-privacidad', false);");
        SybaseJournal.ejecutarQuery("update public.terminosycondiciones set vigente = true where version = '30.0';");
        SybaseJournal.ejecutarQueryConResultado("select id FROM public.terminosycondiciones where version = '30.0';");

        //Comenzamos el flujo del caso
        IngresarALogin ingresarALogin = new IngresarALogin();

        ingresarALogin.validarAperturaDeAplicacion();//step 3
        ingresarALogin.validarObjetosVistaIngresar(true);//step 4
        ingresarALogin.validarURLEnIngresarLogin();// step 5
        ingresarALogin.validarURLBeneficios();//step 6 se pide validación de un solo link
        ingresarALogin.validarAperturaDeAplicacion(); //step 7 Luego se pide ingresar a la aplicación nuevamente
        ingresarALogin.validarObjetosVistaIngresar(true);//step 8
        ingresarALogin.clickIngresar();//step 9 Damos click al boton ingresar
        login.presionarBack ();//step 10 No se abre el teclado
        login.presionarBack ();//step 11
        ingresarALogin.validarAperturaDeAplicacion(); //step 12 Luego se pide ingresar a la aplicación nuevamente
        ingresarALogin.validarObjetosVistaIngresar(true);//step 13
        ingresarALogin.clickIngresar();//step 14 Damos click al boton ingresar
        ingresarALogin.validarObjetosVistaIngresar(true);

        //Ingresamos datos en el login e ingresamos

        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin("10042323-5","111222");

        //Validar links y aceptar terminos y condiciones
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.validarInicio();
        TYC.validarObjetosModalTYC();
        TYC.aceptarTerminosYCondiciones();

    }
}
