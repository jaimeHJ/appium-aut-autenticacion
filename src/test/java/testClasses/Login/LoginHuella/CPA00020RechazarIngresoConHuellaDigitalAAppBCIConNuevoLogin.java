package testClasses.Login.LoginHuella;

import org.aspectj.weaver.ast.Test;
import pages.ActivacionHuellaDigital;
import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;
import testbase.TestBase;

public class CPA00020RechazarIngresoConHuellaDigitalAAppBCIConNuevoLogin extends TestBase {
    public void pruebaAutomatizada() throws Exception {
        //Damos click al boton ingresar
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //Ingresamos datos en el login e ingresamos
        Login login = new Login();
        login.validarInicio();
        login.ingresarDatosLogin("4098198-5","111222");

        //Validar links y aceptar terminos y condiciones
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.validarInicio();
        TYC.aceptarTerminosYCondiciones();

        //Activación huella digital
        ActivacionHuellaDigital pageActivacionHuellaDigital = new ActivacionHuellaDigital();
        pageActivacionHuellaDigital.validarInicio();
        pageActivacionHuellaDigital.habilitarHuella();

        cerrarSesion();

        //Validar huella
    }
}
