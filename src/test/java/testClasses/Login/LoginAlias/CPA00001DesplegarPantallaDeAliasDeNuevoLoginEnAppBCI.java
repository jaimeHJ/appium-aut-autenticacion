package testClasses.Login.LoginAlias;


import pages.MenuLateral;
import pages.IngresarALogin;
import pages.MenuPrincipal;
import reporter.Reports;
import testbase.TestBase;

import static testbase.TestBase.cerrarSesion;
import static testbase.TestBase.loginRapido;


public class CPA00001DesplegarPantallaDeAliasDeNuevoLoginEnAppBCI {

    public void pruebaAutomatizada() throws Exception {

        loginRapido("10042323-5","111222");
        cerrarSesion();

        //Cerramos sesión a través de menú lateral
//        MenuPrincipal menuPrincipal = new MenuPrincipal();
//        menuPrincipal.clickMenuLateral();
//
//        MenuLateral menuLateral = new MenuLateral();
//        menuLateral.clickCerrarSession();

        //Hacemos las validaciones en la vista ingresar
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.validarObjetosVistaIngresar(true);
        ingresarALogin.validarURLEnIngresarLogin();

        Reports.finalAssert();
    }

}
