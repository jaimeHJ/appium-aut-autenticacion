package testClasses.Login.LoginAlias;

import bd.querysTandem;
import pages.*;
import reporter.Reports;
import testbase.TestBase;

public class CPA00007RechazarIngresoconClaveVencida extends TestBase {
    public void pruebaAutomatizada() throws Exception {

        /**
         *INGRESAMOS A TANDEM Y REVERSAMOS VENCIMIENTO USUARIO
         */
        querysTandem.reverzarVencimiento("10042323-5");
        /**
         *INICIAMOS SESSION CON EL LOGIN GENERICO
         */
        loginRapido("10042323-5","111222");

        /**
         *CERRAMOS SESSION A TRAVES DEL MENU LATERAL
         */
        MenuPrincipal menuPrincipal = new MenuPrincipal();
        menuPrincipal.clickMenuLateral();

        MenuLateral menuLateral = new MenuLateral();
        menuLateral.clickCerrarSession();

        /**
         *INGRESAMOS A TANDEM Y VENCEMOS EL USUARIO
         */
        querysTandem.vencerClave("10042323-5");

        /**
         *HACEMOS LA VALIDACIONES Y STEPS EN LA VISTA INGRESAR
         */
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarObjetosVistaIngresar(true);
        ingresarALogin.clickIngresar();

        /**
         *LOGUIN CON ALIAS
         */
        Login login = new Login();
        login.loginConAlias("111222");
        login.presionarIngresar();

        /***************************************************
         *VALIDAMOS MODAL CLAVE VENCIDA
         ****************************************************/
        ModalClaveVencida modalClaveVencida = new ModalClaveVencida();
        modalClaveVencida.validarObjetosModalClaveVencida();
        //ModalClaveBloqueada modalClaveBloqueada = new ModalClaveBloqueada();
        //modalClaveBloqueada.validarObjetosModalClaveBloqueada();

        /***************************************************
         *INGRESAMOS A TANDEM Y REVERSAMOS VENCIMIENTO USUARIO
         ****************************************************/
        querysTandem.reverzarVencimiento("10042323-5");

        Reports.finalAssert();
    }
}
