package testClasses.Login.LoginAlias;

import pages.*;
import bd.querysTandem;
import reporter.Reports;
import testbase.TestBase;

public class CPA00006RechazarIngresoConClaveBloqueadaAppBCIConNuevoLoginAlias extends TestBase {
    public void pruebaAutomatizada() throws Exception {

        /**
         *INGRESAMOS A TANDEM Y DESBLOQUEAMOS USUARIO
         */
        querysTandem.desbloquearClave("10042323-5");

        /**
         *INICIAMOS SESSION CON EL LOGIN GENERICO
         */
        loginRapido("10042323-5","111222");

        /**
         *CERRAMOS SESSION A TRAVES DEL MENU LATERAL
         */
        MenuPrincipal menuPrincipal = new MenuPrincipal();
        menuPrincipal.clickMenuLateral();

        MenuLateral menuLateral = new MenuLateral();
        menuLateral.clickCerrarSession();

        /**
         *INGRESAMOS A TANDEM Y BLOQUEAMOS EL USUARIO
         */
        querysTandem.bloquearClave("10042323-5");

        /**
         *HACEMOS LA VALIDACIONES Y STEPS EN LA VISTA INGRESAR
         */
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarObjetosVistaIngresar(true);
        ingresarALogin.clickIngresar();

        /**
         *LOGUIN CON ALIAS
         */
        Login login = new Login();
        login.loginConAlias("111222");
        login.presionarIngresar();

        /***************************************************
         *VALIDAMOS MODAL CLAVE BLOQUEADA
         ****************************************************/
        ModalClaveBloqueada modalClaveBloqueada = new ModalClaveBloqueada();
        modalClaveBloqueada.validarObjetosModal();
        modalClaveBloqueada.validarURLGenerarClaveBloqueada();

        /***************************************************
         *INGRESAMOS A TANDEM Y DESBLOQUEAMOS USUARIO
         ****************************************************/
        querysTandem.desbloquearClave("10042323-5");

        Reports.finalAssert();
    }
}
