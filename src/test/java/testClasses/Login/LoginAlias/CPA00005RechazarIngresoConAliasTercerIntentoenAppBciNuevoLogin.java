package testClasses.Login.LoginAlias;

import pages.*;
import bd.querysTandem;
import reporter.Reports;
import testbase.TestBase;

public class CPA00005RechazarIngresoConAliasTercerIntentoenAppBciNuevoLogin extends TestBase {
    public void pruebaAutomatizada() throws Exception {

        String rut = "10042323-5";

        /**
         *INGRESAMOS A TANDEM Y DESBLOQUEAMOS USUARIO
         */
        querysTandem.desbloquearClave(rut);

        /**
         *INICIAMOS SESSION CON EL LOGIN GENERICO
         */
        loginRapido(rut,"111222");

        /**
         *CERRAMOS SESSION A TRAVES DEL MENU LATERAL
         */
        MenuPrincipal menuPrincipal = new MenuPrincipal();
        menuPrincipal.clickMenuLateral();

        MenuLateral menuLateral = new MenuLateral();
        menuLateral.clickCerrarSession();

        /**
         *INGRESAMOS A TANDEM Y BLOQUEAMOS EL USUARIO
         */
        //querysTandem.bloquearClave(rut);

        /**
         *HACEMOS LA VALIDACIONES Y STEPS EN LA VISTA INGRESAR
         */
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarObjetosVistaIngresar(true);
        ingresarALogin.clickIngresar();

        /**
         *LOGUIN CON ALIAS
         */
        Login login = new Login();
        login.loginConAlias("11122");
        login.presionarIngresar();

        /**
         * VALIDA Y ACEPTA LOGIN CON DATOS INCORRECTOS
         */

        ModalDatosIngresadosIncorrectos modalDatosIncorrectos = new ModalDatosIngresadosIncorrectos();
        modalDatosIncorrectos.validarObjetosModal();
        modalDatosIncorrectos.presionarAceptar();

        /**
         *  INGRESA DATOS
         */
        login.loginConAlias("11122");
        login.presionarIngresar();

        /**
         * VALIDA Y ACEPTA LOGIN CON DATOS INCORRECTOS
         */
        modalDatosIncorrectos.validarObjetosModal();
        modalDatosIncorrectos.presionarAceptar();

        /**
         *  INGRESA DATOS
         */
        login.loginConAlias("11122");
        login.presionarIngresar();

        /***************************************************
         *VALIDAMOS MODAL CLAVE BLOQUEADA
         ****************************************************/
        ModalClaveBloqueada modalClaveBloqueada = new ModalClaveBloqueada();
        modalClaveBloqueada.validarObjetosModal();

        /***************************************************
         *INGRESAMOS A TANDEM Y DESBLOQUEAMOS USUARIO
         ****************************************************/
        querysTandem.desbloquearClave(rut);

        Reports.finalAssert();
    }
}
