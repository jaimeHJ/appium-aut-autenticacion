package testClasses.Login.LoginAlias;

import pages.*;

import static bd.SybaseJournal.limpiarJournal;
import static bd.SybaseJournal.validarJournal;

public class CPA00002IngresarConAliasAAppBCIConNuevoLogin
{
    public void pruebaAutomatizada() throws InterruptedException
    {
        //Cerramos sesión a través de menú lateral
        MenuPrincipal menuPrincipal = new MenuPrincipal();
        menuPrincipal.clickMenuLateral();

        MenuLateral menuLateral = new MenuLateral();
        menuLateral.clickCerrarSession();

        //Ingresar a LoginNormal
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //LoginNormal
        Login pageLogin = new Login();
        pageLogin.validarInicio();
        pageLogin.validarObjetosVistaLogin(true);
        pageLogin.ingresarDatosLoginConAlias("111222");

        //Menu Principal
        MenuPrincipal pageMenuPrincipal = new MenuPrincipal();
        pageMenuPrincipal.validarInicio();
    }
}
