package testClasses.Login.LoginAlias;
import pages.IngresarALogin;
import pages.Login;
import pages.MenuLateral;
import pages.MenuPrincipal;
import reporter.Reports;
import testbase.TestBase;

public class CPA00011IngresarAAppBCIAlCambiarUsuarioDesdePantallaDeIngresoDeAliasEnNuevoLogin extends TestBase {
    public void pruebaAutomatizada() throws Exception {
        loginRapido("4098198-5","111222");
        cerrarSesion();

        //Ingresar a LoginNormal
        IngresarALogin pageIngresarALogin = new IngresarALogin();
        pageIngresarALogin.validarInicio();
        pageIngresarALogin.clickIngresar();

        //LoginNormal
        Login pageLogin = new Login();
        pageLogin.validarInicio();
        pageLogin.presionarNoEresTu();
        pageLogin.ingresarDatos("4098198-5","111222");

        //Validar LoginNormal con Alias luego de presionar no eres tu

        //pageIngresarALogin.validarObjetosVistaIngresar(true);
        Reports.finalAssert();
    }
}
