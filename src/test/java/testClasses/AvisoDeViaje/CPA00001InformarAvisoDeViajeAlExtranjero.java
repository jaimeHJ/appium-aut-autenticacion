package testClasses.AvisoDeViaje;

import pages.AvisoDeViaje.*;
import pages.IngresarALogin;
import pages.Login;
import pages.MenuPrincipal;
import pages.TerminosYCondiciones;
import pages.Unboxing.AutorizarFaceId;
import pages.Unboxing.MensajeBienvenida;

public class CPA00001InformarAvisoDeViajeAlExtranjero
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "20100921-9";
        String NumContacto = "966666666";
        String Pais1 = "Bolivia";
        String Pais2 = "Argentina";
        String Pais3 = "Brasil";
        String Pais4 = "Volibia";

        //VALIDAR LOGIN.
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //ESCRIBIR DATOS EN EL LOGIN E INGRESAR.
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin(rut, "111222");
        login.dismissAllerts();

        //ACEPTAR TERMINOS Y CONDICIONES.
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.aceptarTyCSinValidar();

        //VALIDAR PANTALLA AUTORIZAR HUELLA Y PRESIONAR AUTORIZAR HUELLA.
        /*AutorizarHuellaDigital autorizarHuellaDigital = new AutorizarHuellaDigital();
        autorizarHuellaDigital.validarObjetosAutorizaeHuella();
        autorizarHuellaDigital.presionarOmitir();*/
        AutorizarFaceId AutorizarFaceId = new AutorizarFaceId();
        AutorizarFaceId.validarObjetosAutorizarFaceID();
        AutorizarFaceId.presionarOmitir();

        //VALIDAR MODAL DE BIENVENIDA.
        MensajeBienvenida MensajeBienvenida = new MensajeBienvenida();
        MensajeBienvenida.validarObjetosBienvenidaDespuesDePresionarOmitirFaceID();
        MensajeBienvenida.presionarIraMiCuenta();

        //INGRESAR A LA OPCION TARJETAS DESDE EL MENU PRINCIPAL.
        MenuPrincipal MenuPrincipal = new MenuPrincipal();
        MenuPrincipal.clickBtnTarjetas();

        //SCROLL HACIA ABAJO Y PRESIONAR BOTON INFORMAR USO AL EXTRANJERO.
        Tarjetas Tarjetas = new Tarjetas();
        Tarjetas.validarMenuTarjetas();
        Tarjetas.ClickBtnInformar();

        // SELECCIONAR PRIMERA TARJETA ACTIVA QUE SE ENCUENTRE EN LA LISTA.
        ListaDeTarjetas ListaDeTarjetas = new ListaDeTarjetas();
        ListaDeTarjetas.validarVistaListaDeTarjetas();
        ListaDeTarjetas.SeleccionarTarjetaActiva();

        //SELECCIONAR FECHAS Y ESCRIBIR EL NUMERO DE CONCACTO.
        InformarUsoEnElExtranjero InformarUsoEnElExtranjero = new InformarUsoEnElExtranjero();
        Calendario Calendario = new Calendario();
        InformarUsoEnElExtranjero.validarVistaInformarUsoEnElExtranjero();
        InformarUsoEnElExtranjero.clickBotonCambiarDesde();
        Calendario.clickSeleccionar();
        InformarUsoEnElExtranjero.clickBotonCambiarHasta();
        Calendario.clickSeleccionar();
        InformarUsoEnElExtranjero.escribirNumeroContacto(NumContacto);
        InformarUsoEnElExtranjero.clickEligeUnooMasPaises();

        //SELECCIONAR Y DESMARCAR PAISES.
        ListaPaises ListaPaises = new ListaPaises();
        ListaPaises.escribirPais(Pais1);
        ListaPaises.clickCancelar();
        ListaPaises.escribirPais(Pais1);
        ListaPaises.clickBotonX();
        InformarUsoEnElExtranjero.clickEligeUnooMasPaises();
        ListaPaises.escribirPais(Pais4); //PAIS NO EXISTENTE.
        ListaPaises.escribirPais(Pais1); //BOLIVIA
        ListaPaises.clickPaisBuscado();
        ListaPaises.escribirPais(Pais2); //ARGEN
        ListaPaises.clickPaisBuscado();
        ListaPaises.escribirPais(Pais3); //BRASIL
        ListaPaises.clickPaisBuscado();
        ListaPaises.escribirPais(Pais1); //BOLIVIA
        ListaPaises.clickPaisBuscado();
        ListaPaises.clickSeleccionar();

        //==============================================================================================================
        // ELIMINAR PAIS CON LA X NO SE PUEDE AUTOMATIZAR===============================================================
        //==============================================================================================================

        //CLICK EN BOTON INFORMAR USO DE TARJETA.
        InformarUsoEnElExtranjero.clickBotonInformarUsoDeTarjeta();

        //CLICK EN BOTON CONFIRMAR DATOS
        RevisaTusDatos RevisaTusDatos = new RevisaTusDatos();
        RevisaTusDatos.clickBtnConfirmarDatos();

        //CLICK EN BOTON ENVIAR OTRO INFORME
        DatosEnviados DatosEnviados = new DatosEnviados();
        DatosEnviados.validarVistaDatosEnviados();
        DatosEnviados.clickBotonEnviarOtroInforme();

        ListaDeTarjetas.validarVistaListaDeTarjetas();
        ListaDeTarjetas.SeleccionarTarjetaActiva();

        //SELECCIONAR FECHAS Y ESCRIBIR EL NUMERO DE CONCACTO.
        InformarUsoEnElExtranjero.clickBotonCambiarDesde();
        Calendario.clickSeleccionar();
        InformarUsoEnElExtranjero.clickBotonCambiarHasta();
        Calendario.clickSeleccionar();
        InformarUsoEnElExtranjero.escribirNumeroContacto(NumContacto);
        InformarUsoEnElExtranjero.clickEligeUnooMasPaises();

        //SELECCIONAR Y DESMARCAR PAISES.
        ListaPaises.escribirPais(Pais1); //BOLI
        ListaPaises.clickPaisBuscado();
        ListaPaises.clickSeleccionar();

        InformarUsoEnElExtranjero.clickBotonInformarUsoDeTarjeta();

        //CLICK EN BOTON CONFIRMAR DATOS
        RevisaTusDatos.clickBtnConfirmarDatos();

        //CLICK BOTON FINALIZAR
        DatosEnviados.validarVistaDatosEnviados();
        DatosEnviados.clickBotonFinalizar();
    }
}
