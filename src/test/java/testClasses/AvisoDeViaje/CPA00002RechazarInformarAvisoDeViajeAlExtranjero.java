package testClasses.AvisoDeViaje;

import pages.AvisoDeViaje.*;
import pages.IngresarALogin;
import pages.Login;
import pages.MenuPrincipal;
import pages.TerminosYCondiciones;
import pages.Unboxing.AutorizarFaceId;
import pages.Unboxing.MensajeBienvenida;

public class CPA00002RechazarInformarAvisoDeViajeAlExtranjero
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "20100921-9";
        String NumContacto = "966666666";
        String Pais1 = "Bolivia";
        String Pais2 = "Argentina";
        String Pais3 = "Brasil";
        String Pais4 = "Volibia";

        //VALIDAR LOGIN.
        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //ESCRIBIR DATOS EN EL LOGIN E INGRESAR.
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin(rut, "111222");
        login.dismissAllerts();

        //ACEPTAR TERMINOS Y CONDICIONES.
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.aceptarTyCSinValidar();

        //VALIDAR PANTALLA AUTORIZAR HUELLA Y PRESIONAR AUTORIZAR HUELLA.
        /*AutorizarHuellaDigital autorizarHuellaDigital = new AutorizarHuellaDigital();
        autorizarHuellaDigital.validarObjetosAutorizaeHuella();
        autorizarHuellaDigital.presionarOmitir();*/
        AutorizarFaceId AutorizarFaceId = new AutorizarFaceId();
        AutorizarFaceId.validarObjetosAutorizarFaceID();
        AutorizarFaceId.presionarOmitir();

        //VALIDAR MODAL DE BIENVENIDA.
        MensajeBienvenida MensajeBienvenida = new MensajeBienvenida();
        MensajeBienvenida.validarObjetosBienvenidaDespuesDePresionarOmitirFaceID();
        MensajeBienvenida.presionarIraMiCuenta();

        //INGRESAR A LA OPCION TARJETAS DESDE EL MENU PRINCIPAL.
        MenuPrincipal MenuPrincipal = new MenuPrincipal();
        MenuPrincipal.clickBtnTarjetas();

        //SCROLL HACIA ABAJO Y PRESIONAR BOTON INFORMAR USO AL EXTRANJERO.
        Tarjetas Tarjetas = new Tarjetas();
        Tarjetas.validarMenuTarjetas();
        Tarjetas.ClickBtnInformar();

        // SELECCIONAR PRIMERA TARJETA ACTIVA QUE SE ENCUENTRE EN LA LISTA.
        ListaDeTarjetas ListaDeTarjetas = new ListaDeTarjetas();
        ListaDeTarjetas.validarVistaListaDeTarjetas();
        ListaDeTarjetas.SeleccionarTarjetaBloqueada();

        //HACER CLICK EN BOTON ENTENDIDO.
        ModalTarjetaBloqueada ModalTarjetaBloqueada = new ModalTarjetaBloqueada();
        ModalTarjetaBloqueada.clickBotonEntendido();
    }
}
