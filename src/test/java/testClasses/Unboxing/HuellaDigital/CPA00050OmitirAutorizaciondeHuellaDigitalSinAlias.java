package testClasses.Unboxing.HuellaDigital;

import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;
import pages.Unboxing.AutorizarHuellaDigital;
import pages.Unboxing.MensajeBienvenida;

public class CPA00050OmitirAutorizaciondeHuellaDigitalSinAlias
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "7222885-5";

        //Limpiar dispositivos en base de datos de un rut
        //Limpiar logs solo en qa
        //PostgreAutenticacionLogin.deleteLogsDispositivos(rut);
        // PostgreAutenticacionLogin.deleteDispositivos(rut);

        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //Ingresamos datos en el login e ingresamos
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin(rut, "111222");
        login.dismissAllerts();

        //Validar links y aceptar terminos y condiciones
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        //TYC.validarInicio();
        //TYC.validarObjetosModalTYC();
        TYC.aceptarTyCSinValidar();

        //valida pantalla autorizar huella y presiona autorizar huella
        AutorizarHuellaDigital autorizarHuellaDigital = new AutorizarHuellaDigital();
        autorizarHuellaDigital.validarObjetosAutorizaeHuella();
        autorizarHuellaDigital.presionarOmitir();

        //validamos Modal de bienvenida

        MensajeBienvenida bienvenida = new MensajeBienvenida();
        bienvenida.validarObjetosBienvenidaDespuesDePresionarOmitir();
        bienvenida.presionarIraMiCuenta();

        //PostgreAutenticacionLogin.dispositivos(rut);
    }
}
