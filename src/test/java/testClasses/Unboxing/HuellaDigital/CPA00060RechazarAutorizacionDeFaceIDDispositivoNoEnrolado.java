package testClasses.Unboxing.HuellaDigital;

import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;
import pages.Unboxing.AutorizarFaceId;
import pages.Unboxing.AutorizarHuellaDigital;
import pages.Unboxing.MensajeBienvenida;
import pages.Unboxing.ModalRegistroFaceId;

public class CPA00060RechazarAutorizacionDeFaceIDDispositivoNoEnrolado
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "10042323-5";

        //Limpiar dispositivos en base de datos de un rut
        //Limpiar logs solo en qa
        //PostgreAutenticacionLogin.deleteLogsDispositivos(rut);
        // PostgreAutenticacionLogin.deleteDispositivos(rut);

        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //Ingresamos datos en el login e ingresamos
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin(rut, "111222");
        login.dismissAllerts();

        //ACEPTAR TERMINOS Y CONDICIONES.
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.aceptarTyCSinValidar();

        //VALIDAR PANTALLA AUTORIZAR FACE ID Y PRESIONAR AUTORIZAR FACE ID.
        AutorizarFaceId AutorizarFaceId = new AutorizarFaceId();
        AutorizarFaceId.validarObjetosAutorizarFaceID();
        AutorizarFaceId.presionarAutorizarFaceID();

        //VALIDAR OBJETOS MODAL REGISTRO FACEID.
        //IR A CONFIGURAR Y VOLVER A APP. LUEGO PRESIONAR OMITIR.
        ModalRegistroFaceId ModalRegistroFaceId = new ModalRegistroFaceId();
        ModalRegistroFaceId.validarObjetosModalRegistroDeHuella();
        ModalRegistroFaceId.presionarConfigurarFaceIDSinFaceIDRegistrada();
/*        ModalRegistroFaceId.presionarOmitirModalRegistroFaceID();

        //VALIDAR MODAL BIENVENIDA.
        MensajeBienvenida MensajeBienvenida = new MensajeBienvenida();
        MensajeBienvenida.validarObjetosBienvenidaDespuesDePresionarOmitirFaceID();*/

        //PostgreAutenticacionLogin.dispositivos(rut);
    }
}
