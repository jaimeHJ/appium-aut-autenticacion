package testClasses.Unboxing.HuellaDigital;

import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;
import pages.Unboxing.AutorizarFaceId;
import pages.Unboxing.MensajeBienvenida;

public class CPA00057AutorizarFaceIDClienteSinAliasRegistrado
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "7222885-5";

        //Limpiar dispositivos en base de datos de un rut
        //Limpiar logs solo en qa
        //PostgreAutenticacionLogin.deleteLogsDispositivos(rut);
        //PostgreAutenticacionLogin.deleteDispositivos(rut);

        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //Ingresamos datos en el login e ingresamos
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin(rut,"111222");
        login.dismissAllerts();

        //VALIDAR LINKS Y ACEPTAR TERMINOS Y CONDICIONES.
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.aceptarTyCSinValidar();

        //VALIDAR PANTALLA AUTORIZAR FACE ID Y PRESIONAR AUTORIZAR FACE ID.
        AutorizarFaceId AutorizarFaceId = new AutorizarFaceId();
        AutorizarFaceId.validarObjetosAutorizarFaceID();
        AutorizarFaceId.presionarAutorizarFaceID();

        //VALIDAR MODAL BIENVENIDA.
        MensajeBienvenida bienvenida = new MensajeBienvenida();
        bienvenida.validarObjetosBienvenidaAutorizarFaceID();
        bienvenida.presionarIraMiCuenta();

        //PostgreAutenticacionLogin.dispositivos(rut);
    }
}
