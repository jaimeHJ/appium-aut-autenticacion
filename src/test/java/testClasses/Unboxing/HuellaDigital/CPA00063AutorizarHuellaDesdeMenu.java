package testClasses.Unboxing.HuellaDigital;

import bd.PostgreAutenticacionLogin;
import pages.*;
import pages.Unboxing.AutorizarHuellaDigital;
import pages.Unboxing.MensajeBienvenida;

public class CPA00063AutorizarHuellaDesdeMenu
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "11995934-9";

        //PostgreAutenticacionLogin.deleteDispositivos(rut);

        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //ESCRIBIR DATOS EN LOGIN E INGRESAR.
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos.");
        login.ingresarDatos(rut,"111222");
        login.dismissAllerts();

        //VALIDA LINK Y ACEPTA TERMINOES Y CONDICIONES.
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.aceptarTyCSinValidar();

        //VALIDAR PANTALLA AUTORIZAR HUELLA Y PRESIONAR OMITIR.
        AutorizarHuellaDigital autorizarHuellaDigital = new AutorizarHuellaDigital();
        autorizarHuellaDigital.validarObjetosAutorizaeHuella();
        autorizarHuellaDigital.presionarOmitir();

        //VALIDAR MENSAJE DE BIENVENIDA DESPUES DE OMITIR.
        MensajeBienvenida bienvenida = new MensajeBienvenida();
        bienvenida.validarObjetosBienvenidaDespuesDePresionarOmitir();
        bienvenida.presionarIraMiCuenta();

        //VALIDAR MEMU PRINCIPAL, CERRAR SESION E INGRESAR NUEVAMENTE.
        MenuPrincipal menuPrincipal = new MenuPrincipal();
        menuPrincipal.clickMenuLateral();

        MenuLateral MenuLateral = new MenuLateral();
        MenuLateral.clickActivarHuella();

        autorizarHuellaDigital.validarObjetosAutorizaeHuellaDesdeMenu();
        autorizarHuellaDigital.presionarAutorizaHuella();
    }
}
