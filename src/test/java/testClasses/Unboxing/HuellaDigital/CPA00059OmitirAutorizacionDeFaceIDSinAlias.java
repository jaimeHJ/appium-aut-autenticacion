package testClasses.Unboxing.HuellaDigital;

import pages.IngresarALogin;
import pages.Login;
import pages.TerminosYCondiciones;
import pages.Unboxing.AutorizarFaceId;
import pages.Unboxing.AutorizarHuellaDigital;
import pages.Unboxing.MensajeBienvenida;

public class CPA00059OmitirAutorizacionDeFaceIDSinAlias
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "7222885-5";

        //Limpiar dispositivos en base de datos de un rut
        //Limpiar logs solo en qa
        //PostgreAutenticacionLogin.deleteLogsDispositivos(rut);
        // PostgreAutenticacionLogin.deleteDispositivos(rut);

        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //Ingresamos datos en el login e ingresamos
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos");
        login.ingresarDatosLogin(rut, "111222");
        login.dismissAllerts();

        //Validar links y aceptar terminos y condiciones
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.aceptarTyCSinValidar();

        //valida pantalla autorizar huella y presiona autorizar huella
        AutorizarFaceId AutorizarFaceId = new AutorizarFaceId();
        AutorizarFaceId.validarObjetosAutorizarFaceID();
        AutorizarFaceId.presionarOmitir();

        //validamos Modal de bienvenida

        MensajeBienvenida bienvenida = new MensajeBienvenida();
        bienvenida.validarObjetosBienvenidaDespuesDePresionarOmitirFaceID();
        bienvenida.presionarIraMiCuenta();

        //PostgreAutenticacionLogin.dispositivos(rut);
    }

}
