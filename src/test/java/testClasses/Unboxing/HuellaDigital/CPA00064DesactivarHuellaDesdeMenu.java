package testClasses.Unboxing.HuellaDigital;

import pages.*;
import pages.Unboxing.AutorizarHuellaDigital;
import pages.Unboxing.MensajeBienvenida;

public class CPA00064DesactivarHuellaDesdeMenu
{
    public void pruebaAutomatizada() throws Exception
    {
        String rut = "11995934-9";

        //PostgreAutenticacionLogin.deleteDispositivos(rut);

        IngresarALogin ingresarALogin = new IngresarALogin();
        ingresarALogin.validarInicio();
        ingresarALogin.clickIngresar();

        //ESCRIBIR DATOS EN LOGIN E INGRESAR.
        Login login = new Login();
        login.validarInicio();
        System.out.println("Ingresando datos.");
        login.ingresarDatos(rut,"111222");
        login.dismissAllerts();

        //VALIDA LINK Y ACEPTA TERMINOES Y CONDICIONES.
        TerminosYCondiciones TYC = new TerminosYCondiciones();
        TYC.aceptarTyCSinValidar();

        //VALIDAR PANTALLA AUTORIZAR HUELLA Y PRESIONAR AUTORIZAR HUELLA.
        AutorizarHuellaDigital autorizarHuellaDigital = new AutorizarHuellaDigital();
        autorizarHuellaDigital.validarObjetosAutorizaeHuella();
        autorizarHuellaDigital.presionarAutorizaHuella();

        //VALIDAR MENSAJE DE BIENVENIDA
        MensajeBienvenida bienvenida = new MensajeBienvenida();
        bienvenida.validarObjetosBienvenida();
        bienvenida.presionarIraMiCuenta();

        //VALIDAR MEMU PRINCIPAL, CERRAR SESION E INGRESAR NUEVAMENTE.
        MenuPrincipal menuPrincipal = new MenuPrincipal();
        menuPrincipal.clickMenuLateral();

        MenuLateral MenuLateral = new MenuLateral();
        MenuLateral.clickDesactivarHuella();
    }
}
